<?php
declare(strict_types=1);

namespace Kia\Entity;

use \Bitrix\Main\Loader;
use \Bitrix\Main\Entity;
use \Bitrix\Iblock\IblockTable;

Loader::includeModule('iblock');

/**
 * @author timofey.semenenko
 *
 * ����� ��� ������ � ��������
 *
 * Class BaseSection
 *
 * @package Kia\Entity
 */
class BaseSection extends \Bitrix\Iblock\SectionTable
{
    protected static $_iblockId = false;
    protected static $_metadata;
    public static $cacheMetadata = true;

    /**
     * getUfId
     */
    public static function getUfId()
    {
        return 'IBLOCK_' . static::$_iblockId . '_SECTION';
    }

    /**
     * getMap
     *
     * @return mixed
     */
    public static function getMap(): array
    {
        static::$_metadata [static::$_iblockId] = static::getMetadata();
        /** @var array $arMap */
        $arMap = parent::getMap();
        $arMap ['LIST_PAGE_URL'] = new Entity\ExpressionField ('LIST_PAGE_URL', "''");
        $arMap ['SECTION_PAGE_URL'] = new Entity\ExpressionField ('SECTION_PAGE_URL', "''");
        return $arMap;
    }

    /**
     * getMetadata
     *
     * @param null $iblockId
     *
     * @return array
     */
    public static function getMetadata($iblockId = null): array
    {
        if (empty ($iblockId)) {
            $iblockId = static::$_iblockId;
        }
        /** @var array $result */
        $result = [];
        /** @var object $obCache */
        $obCache = new \CPHPCache ();
        /** @var string $cacheDir */
        $cacheDir = '/' . $iblockId;
        $result ['iblock'] = IblockTable::getRowById($iblockId);
        $result ['props'] = [];
        return $result;
    }

    /**
     * getList
     *
     * @param $parameters
     *
     * @return \Bitrix\Main\ORM\Query\Result
     */
    public static function getList($parameters): \Bitrix\Main\ORM\Query\Result
    {
        $parameters ['filter'] ['=IBLOCK_ID'] = static::$_iblockId;
        $parameters ['select'] [] = 'IBLOCK_ID';
        /** @var array $rs */
        $rs = parent::getList($parameters);
        $rs->addFetchDataModifier([
            __CLASS__,
            'getFetchDataModifier'
        ]);
        return $rs;
    }

    /**
     * getFetchDataModifier
     *
     * @param $entry
     *
     * @return array
     */
    public static function getFetchDataModifier($entry): array
    {
        /** @var array $fields */
        $fields = [];
        /**
         * @var string $key
         * @var string|int $value
         */
        foreach ($entry as $key => $value) {
            $fields ['~' . $key] = $value;
            $fields [$key] = htmlspecialcharsbx($value);
        }
        $fields ['SECTION_CODE'] = $fields ['CODE'];
        $fields ['SECTION_ID'] = $fields ['ID'];
        if (isset ($fields ['SECTION_PAGE_URL'])) {
            $fields ["~SECTION_PAGE_URL"] = \CIBlock::ReplaceSectionUrl(
                static::$_metadata [$fields ['IBLOCK_ID']] ['iblock'] ['SECTION_PAGE_URL'],
                $fields, true,
                'S'
            );
            $fields ["SECTION_PAGE_URL"] = htmlspecialcharsbx($fields ["~SECTION_PAGE_URL"]);
        }
        if (isset ($fields ['LIST_PAGE_URL'])) {
            $fields ["~LIST_PAGE_URL"] = \CIBlock::ReplaceSectionUrl(
                static::$_metadata [$fields ['IBLOCK_ID']] ['iblock'] ['LIST_PAGE_URL'],
                $fields,
                true
            );
            $fields ["LIST_PAGE_URL"] = htmlspecialcharsbx($fields ["~LIST_PAGE_URL"]);
        }
        return $fields;
    }

    /**
     * getIblockID
     *
     * @return bool
     */
    public static function getIblockID()
    {
        return static::$_iblockId;
    }
}