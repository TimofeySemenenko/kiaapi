<?php
declare(strict_types=1);

namespace Kia\Entity;

use Bitrix\Main\Loader;
use Bitrix\Main\Context;
use Bitrix\Main\Application;

/**
 * @author  timofey.semenenko
 *
 * ��������� ��� Entity
 *
 * Class Constants
 *
 * @package Kia\Entity
 */
class Constants
{
    const
        /** @var string IFRAME */
        IFRAME = '//www.kia.ru/upload/models/panarama/source/frame.html?pano_xml=%2F%2Fwww.kia.ru%2Fupload%2Fmodels%2Fpanarama%2F__xml%2F',
        /** int  CACHE_TIME */
        CACHE_TIME = 3600,
        /** @var int IBLOCK_ID_REVIEWS */
        IBLOCK_ID_REVIEWS = 543,
        /** @var int IBLOCK_ID_PAGES */
        IBLOCK_ID_PAGES = 537,
        /** @var int IBLOCK_ID_GALLERY */
        IBLOCK_ID_GALLERY = 541,
        /** @var int IBLOCK_ID_CHOOSE_BUTTONS */
        IBLOCK_ID_CHOOSE_BUTTONS = 539,
        /** @var int IBLOCK_ID_PROMO */
        IBLOCK_ID_PROMO = 545,
        /** @var int ID_PROP_CHOOSE */
        ID_PROP_CHOOSE = 'CHOOSE_BLOCK',
        /** @var int PROP_CHOOSE_BUTTONS */
        PROP_CHOOSE_BUTTONS_HEADER = 'PRICE_TYPE_BUTTON2',
        /** @var string PROP_CHOOSE_BUTTONS_HELP_BLOCK */
        PROP_CHOOSE_BUTTONS_HELP_BLOCK = 'NEEDHELP_BUTTONS',
        /** @var int IBLOCK_PAGES */
        IBLOCK_COLOR_MODELS = 68,
        /** string PATH_FOR_PRODUCT_SECTION */
        PATH_FOR_PRODUCT_SECTION = '/bitrix/defa_cache/_js/',
        /** int IBLOCK_ID_TEST_CASE */
        IBLOCK_ID_TEST_CASE = 537;
}

