<?php
declare(strict_types=1);

namespace Kia\Entity\Product;

use Bitrix\Main;
use Bitrix\Main\Entity;
use Bitrix\Main\Localization\Loc;
use DEFA\ORM\Iblock\DataModel as DataModel;

/**
 * @author timofey.semenenko
 *
 * Class GalleryTable
 *
 * @package Kia\Entity\Product
 */
class GalleryTable extends DataModel\ADFAORMIblockDataModel
{
    /** @var int $_iblockId */
    static protected $_iblockId = \Kia\Entity\Constants::IBLOCK_ID_GALLERY;

    /**
     * ������� ��.����� "�������"
     *
     * @todo ������� ���������, ���� �������� ������ ��������� $arMap
     *
     * @return array
     */
    public static function getMap(): array
    {
        /** @var array $arMap */
        $arMap = parent::getMap();
        /** @var array $arPropForMap */
        $arPropForMap = [
            'GALLERY_LINK' => 'PROPERTY_GALLERY_LINK',
            'GALLERY_BUTTON_LINK' => 'PROPERTY_GALLERY_BUTTON_LINK',
            'GALLERY_BUTTON_TITLE' => 'PROPERTY_GALLERY_BUTTON_TITLE',
        ];
        /**
         * @var string $key
         * @var string|int|array $prop
         */
        foreach ($arPropForMap as $key => $prop) {
            $arMap [] = new Entity\ExpressionField ($key, '%s', $prop);
        }
        return $arMap;
    }
}
