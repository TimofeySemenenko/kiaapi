<?php
declare(strict_types=1);

namespace Kia\Entity\Product;

use Bitrix\Main;
use Bitrix\Main\Entity;
use Bitrix\Main\Localization\Loc;
use DEFA\ORM\Iblock\DataModel as DataModel;

/**
 * @author timofey.semenenko
 *
 * Class ReviewTable
 *
 * @package Kia\Entity\Product
 */
class ReviewTable extends DataModel\ADFAORMIblockDataModel
{
    static protected $_iblockId = \Kia\Entity\Constants::IBLOCK_ID_REVIEWS;

    /**
     * ������� ��.����� "������"
     *
     * @todo ������� ���������, ���� �������� ������ ��������� $arMap
     *
     * @return array
     */
    public static function getMap(): array
    {
        /** @var array $arMap */
        $arMap = parent::getMap();
        /** @var array $arPropForMap */
        $arPropForMap = [
            'REVIEWS_VIDEO' => 'PROPERTY_REVIEWS_VIDEO',
            'REVIEWS_AUTHOR' => 'PROPERTY_REVIEWS_AUTHOR',
        ];
        /**
         * @var string $key
         * @var string|int|array $prop
         */
        foreach ($arPropForMap as $key => $prop) {
            $arMap [] = new Entity\ExpressionField ($key, '%s', $prop);
        }
        return $arMap;
    }
}

