<?php
declare(strict_types=1);

namespace Kia\Entity\Product;

use Bitrix\Main;
use Bitrix\Main\Entity;
use Bitrix\Main\Localization\Loc;
use DEFA\ORM\Iblock\DataModel as DataModel;

/**
 * @author  timofey.semenenko
 *
 * Class ActionTable
 *
 * @package Kia\Entity\Product
 */
class ActionTable extends DataModel\ADFAORMIblockDataModel
{
    /** @var int $_iblockId */
    static protected $_iblockId = \Kia\Entity\Constants::IBLOCK_ID_PROMO;

    /**
     * Маппинг ин.блока "Акции"
     *
     * @todo сделать эксепшены, если создаётся пустой экземпляр $arMap
     *
     * @return array
     */
    public static function getMap(): array
    {
        /** @var array $arMap */
        $arMap = parent::getMap();
        /** @var array $arPropForMap */
        $arPropForMap = [
            'PROMO_BUTTON_VALUE' => 'PROPERTY_PROMO_BUTTON_VALUE',
            'PROMO_VIDEO_LINK' => 'PROPERTY_PROMO_VIDEO_LINK',
            'PROMO_IMAGE_MOB' => 'PROPERTY_PROMO_IMAGE_MOB',
            'PROMO_IMAGE_DESK' => 'PROPERTY_PROMO_IMAGE_DESK',
        ];

        /**
         * @var string           $key
         * @var string|int|array $prop
         */
        foreach ($arPropForMap as $key => $prop) {
            $arMap [] = new Entity\ExpressionField ($key, '%s', $prop);
        }
        return $arMap;
    }


    /**
     * getFetchDataModifier
     *
     * @todo CFile::GetFileArray переписать на d7
     *
     * @param $fields
     *
     * @return array
     */
    public static function getFetchDataModifier(array $fields): array
    {
        /** @var array $fields */
        $fields = parent::getFetchDataModifier($fields);
        /** @var array $arMeta */
        $arMeta = parent::getMetadata();
        if (( int )$fields ["PROMO_IMAGE_MOB"] > 0) {
            $fields ["PROMO_IMAGE_MOB"] = \CFile::GetFileArray($fields ["PROMO_IMAGE_MOB"]);
        }
        if (( int )$fields ["PROMO_IMAGE_DESK"] > 0) {
            $fields ["PROMO_IMAGE_DESK"] = \CFile::GetFileArray($fields ["PROMO_IMAGE_DESK"]);
        }
        return $fields;
    }
}
