<?php
declare(strict_types=1);

namespace Kia\Entity\Product;

use Bitrix\Main;
use Bitrix\Main\Entity;
use Bitrix\Main\Localization\Loc;
use DEFA\ORM\Iblock\DataModel as DataModel;

/**
 * @author  timofey.semenenko
 *
 * Class PagesTable
 *
 * @package Kia\Entity\Product
 */
class PagesTable extends DataModel\ADFAORMIblockDataModel
{

    static protected $_iblockId = \Kia\Entity\Constants::IBLOCK_ID_PAGES;

    /**
     * ������� ��.����� "��������"
     *
     * getMap
     *
     * @return array
     */
    public static function getMap(): array
    {
        /** @var array $arMap */
        $arMap = parent::getMap();
        /** @var array $arPropForMap */
        $arPropForMap = [
            'HEADER_DESKIMG' => 'PROPERTY_HEADER_DESKIMG',
            'HEADER_MOBIMG' => 'PROPERTY_HEADER_MOBIMG',
            'HEADER_LINK_VIDEO' => 'PROPERTY_HEADER_LINK_VIDEO',
            'HEADER_CHOOSE_POSITION' => 'PROPERTY_HEADER_CHOOSE_POSITION',
            'HEADER_LOGO' => 'PROPERTY_HEADER_LOGO',
            'PRICE_TYPE_MODEL' => 'PROPERTY_PRICE_TYPE_MODEL',
            'PRICE_TYPE_BUTTON1' => 'PROPERTY_PRICE_TYPE_BUTTON1',
            'PRICE_TYPE_BUTTON2' => 'PROPERTY_PRICE_TYPE_BUTTON2',
            'PRICE_PERSENT_VALUE' => 'PROPERTY_PRICE_PERSENT_VALUE',
            'PRICE_VALUE_MONTH' => 'PROPERTY_PRICE_VALUE_MONTH',
            'PRICE_DATA_OF_SALE' => 'PROPERTY_PRICE_DATA_OF_SALE',
            'REVIEWS' => 'PROPERTY_REVIEWS',
            'ENGINE_MODIFICATION' => 'PROPERTY_ENGINE_MODIFICATION',
            'ENGINE_COMPLECTATION' => 'PROPERTY_ENGINE_COMPLECTATION',
            'ENGINE_MAXCOUNT_TORQUE' => 'PROPERTY_ENGINE_COMPLECTATION',
            'ENGINE_MAXCOUNT_POWER' => 'PROPERTY_ENGINE_MAXCOUNT_POWER',
            'NEEDHELP_BUTTONS' => 'PROPERTY_NEEDHELP_BUTTONS',
            'TITLE_BUTTON' => 'PROPERTY_TITLE_BUTTON',
            'SAFETY_FILE' => 'PROPERTY_SAFETY_FILE',
            'SAFETY_BUTTON' => 'PROPERTY_SAFETY_BUTTON',
            'CONTENT_PARALLAX' => 'PROPERTY_CONTENT_PARALLAX',
            'CONTENT_PARALLAX_MOB' => 'PROPERTY_CONTENT_PARALLAX_MOB',
            'CONTENT_GALLERY' => 'PROPERTY_CONTENT_GALLERY',
            'CUSTOM_TITLE_BUTTON' => 'PROPERTY_CUSTOM_TITLE_BUTTON',
            'CUSTOM_URL_BUTTON' => 'PROPERTY_CUSTOM_URL_BUTTON',
            'CUSTOM_CHOOSE_BUTTON' => 'PROPERTY_CUSTOM_CHOOSE_BUTTON',
            'CUSTOM_IMAGE_FILE' => 'PROPERTY_CUSTOM_IMAGE_FILE',
            'ACTION_GALLERY' => 'PROPERTY_ACTION_GALLERY',
            'DEALERS_FILE' => 'PROPERTY_DEALERS_FILE',
            'DEALERS_BUTTON' => 'PROPERTY_DEALERS_BUTTON',
            'FOOTER_BUTTON' => 'PROPERTY_FOOTER_BUTTON',
            'FOOTER_SHARE_BUTTON' => 'PROPERTY_FOOTER_SHARE_BUTTON',
            'FOOTER_LIKE_BUTTON' => 'PROPERTY_FOOTER_LIKE_BUTTON',
            'DEALERS_DEALER_VALUE' => 'PROPERTY_DEALERS_DEALER_VALUE',
            'CHOOSE_BLOCK' => 'PROPERTY_CHOOSE_BLOCK',
            'PRICE_HIDE' => 'PROPERTY_PRICE_HIDE',
            'TEST_CASE' => 'PROPERTY_TEST_CASE',
        ];
        /**
         * @var string           $key
         * @var string|int|array $prop
         */
        foreach ($arPropForMap as $key => $prop) {
            $arMap [] = new Entity\ExpressionField ($key, '%s', $prop);
        }
        return $arMap;
    }

    /**
     * getFetchDataModifier
     *
     * @todo CFile::GetFileArray ���������� �� d7
     *
     * @param $fields
     *
     * @return array
     */
    public static function getFetchDataModifier(array $fields): array
    {
        /** @var array $fields */
        $fields = parent::getFetchDataModifier($fields);
        /** @var array $arMeta */
        $arMeta = parent::getMetadata(); // cached in IblockOrm\ElementTable

        if (( int )$fields ["SAFETY_FILE"] > 0) {
            $fields ["SAFETY_FILE"] = \CFile::GetFileArray($fields ["SAFETY_FILE"]);
        }
        if (( int )$fields ["CONTENT_PARALLAX"] > 0) {
            $fields ["CONTENT_PARALLAX"] = \CFile::GetFileArray($fields ["CONTENT_PARALLAX"]);
        }
        if (( int )$fields ["CONTENT_PARALLAX_MOB"] > 0) {
            $fields ["CONTENT_PARALLAX_MOB"] = \CFile::GetFileArray($fields ["CONTENT_PARALLAX_MOB"]);
        }
        if (( int )$fields ["HEADER_LOGO"] > 0) {
            $fields ["HEADER_LOGO"] = \CFile::GetFileArray($fields ["HEADER_LOGO"]);
        }
        if (( int )$fields ["CUSTOM_IMAGE_FILE"] > 0) {
            $fields ["CUSTOM_IMAGE_FILE"] = \CFile::GetFileArray($fields ["CUSTOM_IMAGE_FILE"]);
        }
        if (( int )$fields ["DEALERS_FILE"] > 0) {
            $fields ["DEALERS_FILE"] = \CFile::GetFileArray($fields ["DEALERS_FILE"]);
        }
        if (( int )$fields ["HEADER_MOBIMG"] > 0) {
            $fields ["HEADER_MOBIMG"] = \CFile::GetFileArray($fields ["HEADER_MOBIMG"]);
        }
        if (( int )$fields ["HEADER_DESKIMG"] > 0) {
            $fields ["HEADER_DESKIMG"] = \CFile::GetFileArray($fields ["HEADER_DESKIMG"]);
        }
        if (( int )$fields ["HEADER_CHOOSE_POSITION"] > 0) {
            $fields ["HEADER_CHOOSE_POSITION"] = self::getListProperty($fields ["HEADER_CHOOSE_POSITION"]);
        }
        if (( int )$fields ["CHOOSE_BLOCK"] > 0) {
            $fields ["CHOOSE_BLOCK"] = self::getListProperty($fields ["CHOOSE_BLOCK"]);
        }
        if (( int )$fields ["CHOOSE_BUTTONS"] > 0) {
            $fields ["CHOOSE_BUTTONS"] = self::getListProperty($fields ["CHOOSE_BUTTONS"]);
        }
        if (( int )$fields ["CHOOSE_COLOR"] > 0) {
            $fields ["CHOOSE_COLOR"] = self::getListProperty($fields ["CHOOSE_COLOR"]);
        }
        return $fields;
    }

    /**
     * getListProperty
     *
     * @param string $field
     *
     * @return string
     */
    protected static function getListProperty(string $field): string
    {
        /** @var array $fieldValueXmlId */
        $fieldValueXmlId = reset(\Bitrix\Iblock\PropertyEnumerationTable::getList([
            'select' => ['XML_ID'],
            'filter' => ['ID' => $field],
            'cache' => [
                'ttl' => \Kia\Entity\Constants::CACHE_TIME,
                'cache_joins' => true,
            ],
        ])->fetchAll());
        return array_pop($fieldValueXmlId);
    }

}
