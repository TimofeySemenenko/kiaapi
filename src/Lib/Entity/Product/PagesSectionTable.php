<?php
declare(strict_types=1);

namespace Kia\Entity\Product;

use Bitrix\Main;
use Bitrix\Main\Entity;
use Bitrix\Main\Localization\Loc;

/**
 * @author  timofey.semenenko
 *
 * Class PagesSectionTable
 *
 * @package Kia\Entity\Product
 */
class PagesSectionTable extends \Kia\Entity\BaseSection
{
    protected static $_iblockId = \Kia\Entity\Constants::IBLOCK_ID_PAGES;
}

