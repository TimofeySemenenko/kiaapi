<?php
declare(strict_types=1);

namespace Kia\Entity\Product;

use Bitrix\Main;
use Bitrix\Main\Entity;
use Bitrix\Main\Localization\Loc;
use DEFA\ORM\Iblock\DataModel as DataModel;

/**
 * @author timofey.semenenko
 *
 * Class ButtonsTable
 *
 * @package Kia\Entity\Product
 *
 */
class ButtonsTable extends DataModel\ADFAORMIblockDataModel
{
    static protected $_iblockId = \Kia\Entity\Constants::IBLOCK_ID_CHOOSE_BUTTONS;

    /**
     * ������� ��.����� "�������� ������"
     *
     * @todo ������� ���������, ���� �������� ������ ��������� $arMap
     *
     * @return array
     */
    public static function getMap(): array
    {
        /** @var array $arMap */
        $arMap = parent::getMap();
        /** @var array $arPropForMap */
        $arPropForMap = [
            'CHOOSE_COLOR' => 'PROPERTY_CHOOSE_COLOR',
            'CHOOSE_BUTTONS' => 'PROPERTY_CHOOSE_BUTTONS',
            'CHOOSE_LINK' => 'PROPERTY_CHOOSE_LINK',
            'CHOOSE_TITLE' => 'PROPERTY_CHOOSE_TITLE',
            'CHOOSE_ICON' => 'PROPERTY_CHOOSE_ICON',
            'CHOOSE_CLASS_BUTTON' => 'PROPERTY_CHOOSE_CLASS_BUTTON',
            'CHOOSE_DOWNLOAD_VALUE' => 'PROPERTY_CHOOSE_DOWNLOAD_VALUE',
            'CHOOSE_TARGET_BLANK_VALUE' => 'PROPERTY_CHOOSE_TARGET_BLANK_VALUE',
        ];
        /**
         * @var string $key
         * @var string|int|array $prop
         */
        foreach ($arPropForMap as $key => $prop) {
            $arMap [] = new Entity\ExpressionField ($key, '%s', $prop);
        }
        return $arMap;
    }
}

