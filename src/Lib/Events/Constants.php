<?php
declare(strict_types=1);

namespace Kia\Events;

/**
 * @author timofey.semenenko
 * ��������� ��� Events
 * Class Constants
 * @package Kia\Events
 */
class Constants
{
    const
        /** @var int IBLOCK_PAGES */
        IBLOCK_PAGES = 538,
        /** @var int ID_PROP_CHOOSE */
        ID_PROP_CHOOSE = 4890,
        /** int  CACHE_TIME */
        CACHE_TIME = 600;
}

