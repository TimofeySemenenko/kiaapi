<?php
declare(strict_types=1);

namespace Kia\Events\Product;

use Bitrix\Main\Loader;
use Bitrix\Main\EventManager;
use Bitrix\Main\Entity\Event;
use Bitrix\Main\SystemException;

/**
 * @author  timofey.semenenko
 *
 * Events Manager ��������� � ����������� ��������
 *
 * Class EventsAdd
 *
 * @package Kia\Events\Product
 */
class EventsAdd
{
    use \Kia\Traits\AdminValidate\Validate;

    /**
     * ����������� �������
     */
    public static function load()
    {
        /** @var object $eventManager */
        $eventManager = EventManager::getInstance();
        $eventManager->addEventHandler("iblock", "OnBeforeIBlockElementAdd", [
            self::class,
            "onBeforeIBlockElementAddPages"
        ]);
    }

    /**
     * ����� ����� �� ���������� �������� �� ����������� �������
     *
     * @param array $arFields
     *
     * @return bool
     */
    public static function onBeforeIBlockElementAddPages(array $arFields)
    {
        try {
            self::productBlocks($arFields);
        } catch (\Exception $objException) {
            global $APPLICATION;
            $APPLICATION->ThrowException($objException->getMessage());
            return false;
        }

    }
}