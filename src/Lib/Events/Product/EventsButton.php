<?php
declare(strict_types=1);

namespace Kia\Events\Product;

use Bitrix\Main\Loader;
use Bitrix\Main\Context;
use Bitrix\Main\Application;
use Bitrix\Main\EventManager;
use Bitrix\Main\Entity\Event;

/**
 * @author  timofey.semenenko
 *
 * Events Manager ��������� � ����������� ��������
 *
 * Class EventsButton
 *
 * @package Kia\Events\Product
 */
class EventsButton
{
    use \Kia\Traits\Commands\RunProductPages;

    /**
     * ����������� �������
     */
    public static function load()
    {
        /** @var object $eventManager */
        $eventManager = EventManager::getInstance();
        $eventManager->addEventHandler("main", "OnAdminListDisplay", [
            self::class,
            "myOnAdminContextMenuShow"
        ]);

    }

    /**
     * ���� - ������ ��� ��������� ����������� �������
     *
     * myOnAdminContextMenuShow
     *
     * @param $list
     */
    public static function myOnAdminContextMenuShow(&$list)
    {
        $request = Context::getCurrent()->getRequest();

        if ($request->get('IBLOCK_ID') == \Kia\Entity\Constants::IBLOCK_ID_PAGES && !empty($request->get('find_section_section'))) {
            /** @var array $fixSection */
            $fixSection = reset((\Kia\Entity\Product\PagesSectionTable::GetList([
                'filter' => [
                    '=CODE' => 'desc',
                    '=ID' => $request->get('find_section_section')
                ],
                'select' => ['DEPTH_LEVEL']
            ]))->fetchAll())['DEPTH_LEVEL'];

            if ($fixSection == 2) {
                /**
                 * @todo   ������ ����!:)
                 * @author trofimov.alexey
                 */
                echo '<a href="' . $GLOBALS["APPLICATION"]->GetCurUri('RERENDER=Y') . '" class="ui-btn-main">
                ���������������� �������� DESC</a><br><br>';

                if ($request->get('RERENDER') == 'Y') {
                    /**
                     * @todo ����� �� �������  api, refactor
                     * @var array $sectionId
                     */
                    $arSectionInfo = (\CIBlockSection::GetByID($request->get('find_section_section')))->fetch();
                    /** @var string $codeFirstSection */
                    $codeFirstSection = reset((\Kia\Entity\Product\PagesSectionTable::GetList([
                        'filter' => ['=ID' => $arSectionInfo['IBLOCK_SECTION_ID']],
                        'select' => ['CODE']
                    ]))->fetchAll())['CODE'];
                    if (self::runGeneratePages(['model' => $codeFirstSection, 'page' => $arSectionInfo['CODE']])) {
                        \CAdminMessage::ShowMessage(array(
                            'MESSAGE' => '����������������',
                            'DETAILS' => '������������� �������� '
                                . strtoupper($arSectionInfo['CODE']) . ' ����������� �������!',
                            'HTML' => true,
                            'TYPE' => 'OK',
                        ));
                    } else {
                        \CAdminMessage::ShowMessage(array(
                            'MESSAGE' => '����������������',
                            'DETAILS' => '������������� �������� '
                                . strtoupper($arSectionInfo['CODE']) . ' ����������� �� �������!',
                            'HTML' => true,
                            'TYPE' => 'ERROR',
                        ));
                    }
                }
            }
        }
    }

}