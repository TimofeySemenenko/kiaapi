<?php
declare(strict_types=1);

namespace Kia\Events\Product;

use Bitrix\Main\Loader;
use Bitrix\Main\EventManager;
use Bitrix\Main\Entity\Event;

/**
 * @author  timofey.semenenko
 *
 * Events Manager ��������� � ����������� ��������
 *
 * Class EventsTools
 *
 * @package Kia\Events\Product
 */
class EventsTools
{
    /**
     * ����������� �������
     */
    public static function load()
    {
        /** @var object $eventManager */
        $eventManager = EventManager::getInstance();
        $eventManager->addEventHandler("main", "OnAdminTabControlBegin", [
            self::class,
            "OnAdminTabControlBegin"
        ]);
    }

    /**
     * ������� ������ ������������ ����� ��� ����������
     *
     * OnAdminTabControlBegin
     *
     * @param $formData
     */
    public static function OnAdminTabControlBegin(&$formData)
    {
        \CJSCore::Init(["jquery"]);
        ob_start();
        /** @var array $arResultAll */
        $arResultAll = \Bitrix\Iblock\PropertyTable::getList([
            'select' => [
                'CODE',
                'ID',

            ],
            'filter' => [
                'IBLOCK_ID' => \Kia\Entity\Constants::IBLOCK_ID_PAGES,
                'ACTIVE' => 'Y'
            ],
            'cache' => [
                'ttl' => \Kia\Entity\Constants::CACHE_TIME,
                'cache_joins' => true,
            ],

        ]);
        /** @var array $resultProp */
        $resultProp = $arResultAll->fetchAll();
        /** @var array $result */
        $resultPropAllObj = [];
        /** @var string $item */
        foreach ($resultProp as $item) {
            $resultPropAllObj[$item['CODE']] = $item['ID'];
        }
        /** @var array $arAllProp */
        $arAllProp = json_encode($resultPropAllObj);
        /** @var array $arResultChoose */
        $arResultChoose = \Bitrix\Iblock\PropertyEnumerationTable::getList([
            'select' => [
                'XML_ID',
                'ID',
            ],
            'filter' => [
                'PROPERTY_ID' => \CDFAIBlockTools::GetPropertyIDByCode(\Kia\Entity\Constants::ID_PROP_CHOOSE, \Kia\Entity\Constants::IBLOCK_ID_PAGES),
            ],
            'cache' => [
                'ttl' => \Kia\Entity\Constants::CACHE_TIME,
                'cache_joins' => true,
            ],

        ]);
        /** @var array $resultChoose */
        $resultChoose = $arResultChoose->fetchAll();
        /** @var array $resultPropObj */
        $resultPropObj = [];
        /** @var string $item */
        foreach ($resultChoose as $item) {
            $resultPropObj[$item['XML_ID']] = $item['ID'];
        }
        /** @var array $arChoose */
        $arChoose = json_encode($resultPropObj, JSON_HEX_QUOT);
        ?>
        <script>
            var mapValueSelect = JSON.parse('<?=$arChoose?>'),
                mapTypes = JSON.parse('<?=$arAllProp?>');
            var adminStaticData = {
                init: function () {
                    var prop = '<?= \CDFAIBlockTools::GetPropertyIDByCode(\Kia\Entity\Constants::ID_PROP_CHOOSE, \Kia\Entity\Constants::IBLOCK_ID_PAGES)?>';
                    var _this = this;
                    var type = $('#tr_PROPERTY_' + prop).find('select').val();
                    console.log(prop);
                    $(document).on('change', '#tr_PROPERTY_' + prop + ' select', function () {
                        type = $(this).val();
                        _this.change(type);
                    });
                    if (type) {
                        _this.change(type);
                    }
                },
                headerDesktopImg: function (typeId) {
                    var obHeaderDesktopImg = $('#tr_PROPERTY_' + mapTypes.HEADER_DESKIMG);
                    var arShowTypes = [
                        mapValueSelect.header,
                    ];
                    return (arShowTypes.indexOf(typeId) != '-1') ? obHeaderDesktopImg.show() : obHeaderDesktopImg.hide();
                },

                headerPhoneImg: function (typeId) {
                    var obHeaderLinkToYouTube = $('#tr_PROPERTY_' + mapTypes.HEADER_MOBIMG);
                    var arShowTypes = [
                        mapValueSelect.header,
                    ];
                    return (arShowTypes.indexOf(typeId) != '-1') ? obHeaderLinkToYouTube.show() : obHeaderLinkToYouTube.hide();
                },
                headerLinkToYouTube: function (typeId) {
                    var obHeaderLinkToYouTube = $('#tr_PROPERTY_' + mapTypes.HEADER_LINK_VIDEO);
                    var arShowTypes = [
                        mapValueSelect.header,
                    ];
                    return (arShowTypes.indexOf(typeId) != '-1') ? obHeaderLinkToYouTube.show() : obHeaderLinkToYouTube.hide();
                },
                headerChoosePosition: function (typeId) {
                    var obHeaderLinkToYouTube = $('#tr_PROPERTY_' + mapTypes.HEADER_CHOOSE_POSITION);
                    var arShowTypes = [
                        mapValueSelect.header,
                    ];
                    return (arShowTypes.indexOf(typeId) != '-1') ? obHeaderLinkToYouTube.show() : obHeaderLinkToYouTube.hide();
                },
                headerAddLogo: function (typeId) {
                    var obHeaderAddLogo = $('#tr_PROPERTY_' + mapTypes.HEADER_LOGO);
                    var arShowTypes = [
                        mapValueSelect.header,
                    ];
                    return (arShowTypes.indexOf(typeId) != '-1') ? obHeaderAddLogo.show() : obHeaderAddLogo.hide();
                },
                //start
                priceDataOfSale: function (typeId) {
                    var obPriceDataOfSale = $('#tr_PROPERTY_' + mapTypes.PRICE_DATA_OF_SALE);
                    var arShowTypes = [
                        mapValueSelect.topPriceBlockWithCreditOffer,
                    ];
                    return (arShowTypes.indexOf(typeId) != '-1') ? obPriceDataOfSale.show() : obPriceDataOfSale.hide();
                },
                priceTypeModel: function (typeId) {
                    var obPriceTypeEngine = $('#tr_PROPERTY_' + mapTypes.PRICE_TYPE_MODEL);
                    var arShowTypes = [
                        mapValueSelect.topPriceBlockWithOneButton,
                        mapValueSelect.topPriceBlockWithButtons,
                        mapValueSelect.topPriceBlockStartToSales,
                        mapValueSelect.topPriceBlockWithCreditOffer,

                    ];
                    return (arShowTypes.indexOf(typeId) != '-1') ? obPriceTypeEngine.show() : obPriceTypeEngine.hide();
                },
                priceHide: function (typeId) {
                    var obPriceHide = $('#tr_PROPERTY_' + mapTypes.PRICE_HIDE);
                    var arShowTypes = [
                        mapValueSelect.topPriceBlockWithOneButton,
                        mapValueSelect.topPriceBlockWithButtons,
                        mapValueSelect.topPriceBlockStartToSales,
                        mapValueSelect.topPriceBlockWithCreditOffer,
                    ];
                    return (arShowTypes.indexOf(typeId) != '-1') ? obPriceHide.show() : obPriceHide.hide();
                },
                priceTypeButton1: function (typeId) {
                    var obPriceTypeButton1 = $('#tr_PROPERTY_' + mapTypes.PRICE_TYPE_BUTTON1);
                    var arShowTypes = [
                        mapValueSelect.topPriceBlockWithOneButton,
                        mapValueSelect.topPriceBlockWithCreditOffer,
                    ];
                    return (arShowTypes.indexOf(typeId) != '-1') ? obPriceTypeButton1.show() : obPriceTypeButton1.hide();
                },
                priceTypeButton2: function (typeId) {
                    var obPriceTypeButton2 = $('#tr_PROPERTY_' + mapTypes.PRICE_TYPE_BUTTON2);
                    var arShowTypes = [
                        mapValueSelect.topPriceBlockWithButtons,
                    ];
                    return (arShowTypes.indexOf(typeId) != '-1') ? obPriceTypeButton2.show() : obPriceTypeButton2.hide();
                },
                pricePercentValue: function (typeId) {
                    var obPricePercentValue = $('#tr_PROPERTY_' + mapTypes.PRICE_PERSENT_VALUE);
                    var arShowTypes = [
                        mapValueSelect.topPriceBlockStartToSales,
                        mapValueSelect.topPriceBlockWithOneButton,
                    ];
                    return (arShowTypes.indexOf(typeId) != '-1') ? obPricePercentValue.show() : obPricePercentValue.hide();
                },
                priceValueMonth: function (typeId) {
                    var obPriceValueMonth = $('#tr_PROPERTY_' + mapTypes.PRICE_VALUE_MONTH);
                    var arShowTypes = [
                        mapValueSelect.topPriceBlockStartToSales,
                        mapValueSelect.topPriceBlockWithOneButton,
                    ];
                    return (arShowTypes.indexOf(typeId) != '-1') ? obPriceValueMonth.show() : obPriceValueMonth.hide();
                },
                //end
                reviewsProduct: function (typeId) {
                    var obReviewsProduct = $('#tr_PROPERTY_' + mapTypes.REVIEWS);
                    var arShowTypes = [
                        mapValueSelect.reviews,
                    ];
                    return (arShowTypes.indexOf(typeId) != '-1') ? obReviewsProduct.show() : obReviewsProduct.hide();
                },
                engineModification: function (typeId) {
                    var obEngineModification = $('#tr_PROPERTY_' + mapTypes.ENGINE_MODIFICATION);
                    var arShowTypes = [
                        mapValueSelect.engines,
                    ];
                    return (arShowTypes.indexOf(typeId) != '-1') ? obEngineModification.show() : obEngineModification.hide();
                },
                engineComplectation: function (typeId) {
                    var obEngineComplectation = $('#tr_PROPERTY_' + mapTypes.ENGINE_COMPLECTATION);
                    var arShowTypes = [
                        mapValueSelect.engines,
                    ];
                    return (arShowTypes.indexOf(typeId) != '-1') ? obEngineComplectation.show() : obEngineComplectation.hide();
                },
                engineMaxCountTorque: function (typeId) {
                    var onEngineMaxCountTorque = $('#tr_PROPERTY_' + mapTypes.ENGINE_MAXCOUNT_TORQUE);
                    var arShowTypes = [
                        mapValueSelect.engines,
                    ];
                    return (arShowTypes.indexOf(typeId) != '-1') ? onEngineMaxCountTorque.show() : onEngineMaxCountTorque.hide();
                },
                engineMaxCountPower: function (typeId) {
                    var obEngineMaxCountPower = $('#tr_PROPERTY_' + mapTypes.ENGINE_MAXCOUNT_POWER);
                    var arShowTypes = [
                        mapValueSelect.engines,
                    ];
                    return (arShowTypes.indexOf(typeId) != '-1') ? obEngineMaxCountPower.show() : obEngineMaxCountPower.hide();
                },
                needHelpButtons: function (typeId) {
                    var obNeedHelpButtons = $('#tr_PROPERTY_' + mapTypes.NEEDHELP_BUTTONS);
                    var arShowTypes = [
                        mapValueSelect.helpBlock,
                    ];
                    return (arShowTypes.indexOf(typeId) != '-1') ? obNeedHelpButtons.show() : obNeedHelpButtons.hide();
                },
                titleName: function (typeId) {
                    var obTitleName = $('#tr_PROPERTY_' + mapTypes.TITLE_NAME);
                    var arShowTypes = [
                        mapValueSelect.textBlock,
                    ];
                    return (arShowTypes.indexOf(typeId) != '-1') ? obTitleName.show() : obTitleName.hide();
                },
                titleButton: function (typeId) {
                    var obTitleButton = $('#tr_PROPERTY_' + mapTypes.TITLE_BUTTON);
                    var arShowTypes = [
                        mapValueSelect.textBlock,
                    ];
                    return (arShowTypes.indexOf(typeId) != '-1') ? obTitleButton.show() : obTitleButton.hide();
                },
                safetyFile: function (typeId) {
                    var obSafetyFile = $('#tr_PROPERTY_' + mapTypes.SAFETY_FILE);
                    var arShowTypes = [
                        mapValueSelect.safety,
                    ];
                    return (arShowTypes.indexOf(typeId) != '-1') ? obSafetyFile.show() : obSafetyFile.hide();
                },
                safetyText: function (typeId) {
                    var obSafetyText = $('#tr_PROPERTY_' + mapTypes.SAFETY_TEXT);
                    var arShowTypes = [
                        mapValueSelect.safety,
                    ];
                    return (arShowTypes.indexOf(typeId) != '-1') ? obSafetyText.show() : obSafetyText.hide();
                },
                safetyButton: function (typeId) {
                    var obSafetyButton = $('#tr_PROPERTY_' + mapTypes.SAFETY_BUTTON);
                    var arShowTypes = [
                        mapValueSelect.safety,
                    ];
                    return (arShowTypes.indexOf(typeId) != '-1') ? obSafetyButton.show() : obSafetyButton.hide();
                },
                contentParallax: function (typeId) {
                    var obContentParallax = $('#tr_PROPERTY_' + mapTypes.CONTENT_PARALLAX);
                    var arShowTypes = [
                        mapValueSelect.parallax,
                    ];
                    return (arShowTypes.indexOf(typeId) != '-1') ? obContentParallax.show() : obContentParallax.hide();
                },
                contentParallaxVideo: function (typeId) {
                    var obContentParallax = $('#tr_PROPERTY_' + mapTypes.CONTENT_PARALLAX_MOB);
                    var arShowTypes = [
                        mapValueSelect.parallax,
                    ];
                    return (arShowTypes.indexOf(typeId) != '-1') ? obContentParallax.show() : obContentParallax.hide();
                },
                contentImage: function (typeId) {
                    var obContentImage = $('#tr_PROPERTY_' + mapTypes.CONTENT_IMAGE);
                    var arShowTypes = [
                        mapValueSelect.textImgBlock,
                        mapValueSelect.textImgBlockReverse,
                    ];
                    return (arShowTypes.indexOf(typeId) != '-1') ? obContentImage.show() : obContentImage.hide();
                },
                contentTitle: function (typeId) {
                    var obContentTitle = $('#tr_PROPERTY_' + mapTypes.CONTENT_DESCRIPTION);
                    var arShowTypes = [
                        mapValueSelect.textImgBlock,
                        mapValueSelect.textImgBlockReverse,
                    ];
                    return (arShowTypes.indexOf(typeId) != '-1') ? obContentTitle.show() : obContentTitle.hide();
                },
                contentDescription: function (typeId) {
                    var obContentDescription = $('#tr_PROPERTY_' + mapTypes.CONTENT_TITLE);
                    var arShowTypes = [
                        mapValueSelect.textImgBlock,
                        mapValueSelect.textImgBlockReverse,
                    ];
                    return (arShowTypes.indexOf(typeId) != '-1') ? obContentDescription.show() : obContentDescription.hide();
                },
                contentGallery: function (typeId) {
                    var obContentGallery = $('#tr_PROPERTY_' + mapTypes.CONTENT_GALLERY);
                    var arShowTypes = [
                        mapValueSelect.textImageGallery,
                    ];
                    return (arShowTypes.indexOf(typeId) != '-1') ? obContentGallery.show() : obContentGallery.hide();
                },
                customTitleButton: function (typeId) {
                    var obCustomTitleButton = $('#tr_PROPERTY_' + mapTypes.CUSTOM_TITLE_BUTTON);
                    var arShowTypes = [
                        mapValueSelect.customBlock,
                    ];
                    return (arShowTypes.indexOf(typeId) != '-1') ? obCustomTitleButton.show() : obCustomTitleButton.hide();
                },
                customUrlButton: function (typeId) {
                    var obCustomUrlButton = $('#tr_PROPERTY_' + mapTypes.CUSTOM_URL_BUTTON);
                    var arShowTypes = [
                        mapValueSelect.customBlock,
                    ];
                    return (arShowTypes.indexOf(typeId) != '-1') ? obCustomUrlButton.show() : obCustomUrlButton.hide();
                },
                customChooseButton: function (typeId) {
                    var obCustomChooseButton = $('#tr_PROPERTY_' + mapTypes.CUSTOM_CHOOSE_BUTTON);
                    var arShowTypes = [
                        mapValueSelect.customBlock,
                    ];
                    return (arShowTypes.indexOf(typeId) != '-1') ? obCustomChooseButton.show() : obCustomChooseButton.hide();
                },
                customImageFile: function (typeId) {
                    var obCustomImageFile = $('#tr_PROPERTY_' + mapTypes.CUSTOM_IMAGE_FILE);
                    var arShowTypes = [
                        mapValueSelect.customBlock,
                    ];
                    return (arShowTypes.indexOf(typeId) != '-1') ? obCustomImageFile.show() : obCustomImageFile.hide();
                },
                actionGallery: function (typeId) {
                    var obActionGallery = $('#tr_PROPERTY_' + mapTypes.ACTION_GALLERY);
                    var arShowTypes = [
                        mapValueSelect.saleBlock,
                    ];
                    return (arShowTypes.indexOf(typeId) != '-1') ? obActionGallery.show() : obActionGallery.hide();
                },
                dealersFile: function (typeId) {
                    var obDealersFile = $('#tr_PROPERTY_' + mapTypes.DEALERS_FILE);
                    var arShowTypes = [
                        mapValueSelect.dealerContentBlock,
                    ];
                    return (arShowTypes.indexOf(typeId) != '-1') ? obDealersFile.show() : obDealersFile.hide();
                },
                dealersButton: function (typeId) {
                    var obDealersButton = $('#tr_PROPERTY_' + mapTypes.DEALERS_BUTTON);
                    var arShowTypes = [
                        mapValueSelect.dealerContentBlock,
                    ];
                    return (arShowTypes.indexOf(typeId) != '-1') ? obDealersButton.show() : obDealersButton.hide()
                },
                dealersProduct: function (typeId) {
                    var obDealersProduct = $('#tr_PROPERTY_' + mapTypes.DEALERS_DEALER_VALUE);
                    var arShowTypes = [
                        mapValueSelect.dealerContentBlock,
                    ];
                    return (arShowTypes.indexOf(typeId) != '-1') ? obDealersProduct.show() : obDealersProduct.hide();
                },
                footerButton: function (typeId) {
                    var obFooterButton = $('#tr_PROPERTY_' + mapTypes.FOOTER_BUTTON);
                    var arShowTypes = [
                        mapValueSelect.footerInfo,
                    ];
                    return (arShowTypes.indexOf(typeId) != '-1') ? obFooterButton.show() : obFooterButton.hide();
                },
                footerShare: function (typeId) {
                    var obFooterDisclaimer = $('#tr_PROPERTY_' + mapTypes.FOOTER_SHARE_BUTTON);
                    var arShowTypes = [
                        mapValueSelect.footerInfo,
                    ];
                    return (arShowTypes.indexOf(typeId) != '-1') ? obFooterDisclaimer.show() : obFooterDisclaimer.hide();
                },
                footerLike: function (typeId) {
                    var obFooterDisclaimer = $('#tr_PROPERTY_' + mapTypes.FOOTER_LIKE_BUTTON);
                    var arShowTypes = [
                        mapValueSelect.footerInfo,
                    ];
                    return (arShowTypes.indexOf(typeId) != '-1') ? obFooterDisclaimer.show() : obFooterDisclaimer.hide();
                },

                change: function (type) {
                    this.headerDesktopImg(type);
                    this.headerPhoneImg(type);
                    this.headerLinkToYouTube(type);
                    this.headerChoosePosition(type);
                    this.headerAddLogo(type);
                    // ������ "���������"
                    this.priceDataOfSale(type);
                    this.priceTypeModel(type);
                    this.priceTypeButton1(type);
                    this.priceTypeButton2(type);
                    this.pricePercentValue(type);
                    this.priceValueMonth(type);
                    this.priceHide(type);
                    // ������ "������"
                    this.reviewsProduct(type);
                    // ������ "���������"
                    this.engineModification(type);
                    this.engineComplectation(type);
                    this.engineMaxCountTorque(type);
                    this.engineMaxCountPower(type);
                    // ������ "����� ������"
                    this.needHelpButtons(type);
                    // ������ "���������"
                    this.titleName(type);
                    this.titleButton(type);
                    // ������ "������������"
                    this.safetyFile(type);
                    this.safetyText(type);
                    this.safetyButton(type);
                    // ������ "�������"
                    this.contentParallax(type);
                    this.contentParallaxVideo(type);
                    this.contentImage(type);
                    this.contentTitle(type);
                    this.contentDescription(type);
                    this.contentGallery(type);
                    // ������ "���������"
                    this.customTitleButton(type);
                    this.customUrlButton(type);
                    this.customChooseButton(type);
                    this.customImageFile(type);
                    // ������ "�����"
                    this.actionGallery(type);
                    // ������ "������"
                    this.dealersFile(type);
                    this.dealersButton(type);
                    this.dealersProduct(type);
                    // ������ "�����"
                    this.footerButton(type);
                    this.footerShare(type);
                    this.footerLike(type);
                },
            };
            adminStaticData.init();
        </script><?php
        /** @var object $sEpilogContent */
        $sEpilogContent = ob_get_contents();
        ob_end_clean();
        $formData->sEpilogContent .= $sEpilogContent;

    }
}