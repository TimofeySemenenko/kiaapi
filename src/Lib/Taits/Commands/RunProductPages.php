<?php
declare(strict_types=1);

namespace Kia\Taits\Commands;

use Symfony\Component\Process\Process;

/**
 * @author timofey.semenenko
 * Trait RunProductPages
 * @package Kia\Taits\Commands
 */
trait RunProductPages
{
    /**
     * Генерация страниц продуктового раздела
     * runGeneratePages
     * @param array $arRes
     */
    public static function runGeneratePages(array $arRes) :bool
    {
        try {
            /** @var Process $process */
            $process = new Process(
                "cd /home/bitrix/dev01/dev/html/uveu_render/ && node generate.js '" . json_encode($arRes) . "'"
            );
            $process->run();
            if (!$process->isSuccessful()) {
                throw new \Exception($process);
            }
            return true;
        } catch (\Exception $objException) {
            return false;
        }
    }
}
