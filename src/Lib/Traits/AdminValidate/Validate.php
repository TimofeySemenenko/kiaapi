<?php
declare(strict_types=1);

namespace Kia\Traits\AdminValidate;

use Bitrix\Main\Loader;
use Bitrix\Main\EventManager;
use Bitrix\Main\Entity\Event;
use Bitrix\Main\SystemException;

/**
 * ��������� ��� ������ ����������� �������
 *
 * @author  timofey.semenenko
 *
 * Trait Validate
 *
 * @package Kia\Taits\AdminValidate
 */
trait Validate
{
    /**
     * productBlocks
     *
     * @param array $arFields
     *
     * @throws \Exception
     */
    public static function productBlocks(array &$arFields)
    {
        if ($arFields['IBLOCK_ID'] == \Kia\Entity\Constants::IBLOCK_ID_PAGES) {
            /** @var array $arBlocks */
            $arBlocks = \Kia\Entity\Product\PagesTable::GetList([
                'select' => ['CHOOSE_BLOCK', 'ID'],
                'filter' => ['IBLOCK_SECTION_ID' => $arFields['IBLOCK_SECTION'][0]]
            ])->fetchAll();
            /** @var array $valueProp */
            $valueProp = array_shift($arFields['PROPERTY_VALUES'][\CDFAIBlockTools::GetPropertyIDByCode(\Kia\Entity\Constants::ID_PROP_CHOOSE, \Kia\Entity\Constants::IBLOCK_ID_PAGES)]);
            /** @var array $arResultChoose */
            $arResultChoose = reset(\Bitrix\Iblock\PropertyEnumerationTable::getList([
                'select' => [
                    'XML_ID',
                ],
                'filter' => [
                    'ID' => $valueProp['VALUE'],
                ],
                'cache' => [
                    'ttl' => \Kia\Entity\Constants::CACHE_TIME,
                    'cache_joins' => true,
                ],

            ])->fetchAll());
            /** @todo ���������� �� global */
            global $APPLICATION;
            switch ($arResultChoose['XML_ID']) {
                case 'topPriceBlockWithButtons':
                    /** @var array $valuePropWithButtons */
                    $valuePropWithButtons = count(array_filter(array_map('array_filter', $arFields['PROPERTY_VALUES'][\CDFAIBlockTools::GetPropertyIDByCode(
                        \Kia\Entity\Constants::PROP_CHOOSE_BUTTONS_HEADER, \Kia\Entity\Constants::IBLOCK_ID_PAGES
                    )])));
                    if ($valuePropWithButtons > 2) {
                        throw new \Exception('� ����� ' . strtoupper($arResultChoose['XML_ID']) . ' ������ ���� ��������� �� ����� 2 ������!');
                    }
                    break;
                case 'helpBlock':
                    /** @var array $valuePropHelpBlock */
                    $valuePropHelpBlock = count(array_filter(array_map('array_filter', $arFields['PROPERTY_VALUES'][\CDFAIBlockTools::GetPropertyIDByCode(
                        \Kia\Entity\Constants::PROP_CHOOSE_BUTTONS_HELP_BLOCK, \Kia\Entity\Constants::IBLOCK_ID_PAGES)]
                    )));;
                    if ($valuePropHelpBlock > 2) {
                        throw new \Exception('� ����� ' . strtoupper($arResultChoose['XML_ID']) . ' ������ ���� ��������� �� ����� 2 ������!');
                    }
                    break;
                case 'header':
                    $arFields['SORT'] = 1;
                    foreach ($arBlocks as $valueBlock) {
                        if ($valueBlock['CHOOSE_BLOCK'] == $arResultChoose['XML_ID'] && $valueBlock['ID'] != $arFields['ID']) {
                            throw new \Exception('���� ' . strtoupper($arResultChoose['XML_ID']) . ' ��� ��� ������!');
                        }
                    }
                    break;
                case 'footerInfo':
                    $arFields['SORT'] = 10000;
                    foreach ($arBlocks as $valueBlock) {
                        if ($valueBlock['CHOOSE_BLOCK'] == $arResultChoose['XML_ID'] && $valueBlock['ID'] != $arFields['ID']) {
                            throw new \Exception('���� ' . strtoupper($arResultChoose['XML_ID']) . ' ��� ��� ������!');
                        }
                    }
                    break;
                case 'headerMenuBlock':
                    $arFields['SORT'] = 2;
                    foreach ($arBlocks as $valueBlock) {
                        if ($valueBlock['CHOOSE_BLOCK'] == $arResultChoose['XML_ID'] && $valueBlock['ID'] != $arFields['ID']) {

                            throw new \Exception('���� ' . strtoupper($arResultChoose['XML_ID']) . ' ��� ��� ������!');
                        }
                    }
                    break;
            }
            unset($arBlocks);
            unset($valueProp);
            unset($arResultChoose);
        }
    }

}