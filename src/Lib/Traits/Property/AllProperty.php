<?php
declare(strict_types=1);

namespace Kia\Traits\Property;

use Bitrix\Main\Loader;
use Bitrix\Main\EventManager;
use Bitrix\Main\Entity\Event;

/**
 * @author  timofey.semenenko
 *
 * ��� ������ �� ���������� ������ ���������
 *
 * AllProperty
 *
 * Trait AllProperty
 *
 * @package Kia\Traits\Property
 */
trait AllProperty
{
    /**
     * getAllProp
     *
     * @param int $iblockId
     *
     * @return array
     */
    public function getAllProp(int $iblockId): array
    {
        /** @var array $arRes */
        $arRes = [];
        /** @var array $arProp */
        $arProp = (\Bitrix\Iblock\PropertyTable::getList([
            'select' => [
                'CODE',
            ],
            'filter' => [
                'IBLOCK_ID' => $iblockId,
            ],
            'cache' => [
                'ttl' => \Kia\Entity\Constants::CACHE_TIME,
                'cache_joins' => true,
            ],

        ]))->fetchAll();
        /** @var array $value */
        foreach ($arProp as $value) {
            $arRes[] = $value['CODE'];
        }
        return $arRes;

    }

}