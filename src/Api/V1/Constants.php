<?php
declare(strict_types=1);

namespace Api\V1;

/**
 * @author timofey.semenenko
 * ��������� ��� Events
 * Class Constants
 * @package Kia\Events
 */
class Constants
{
    const
        /** int  CACHE_TIME */
        CACHE_TIME = 3600,
        /** @var int IBLOCK_ID_PAGES */
        IBLOCK_ID_PAGES = 538,
        /** @var int IBLOCK_PAGES */
        IBLOCK_COLOR_MODELS = 68,
        /** string PATH_FOR_PRODUCT_SECTION */
        PATH_FOR_PRODUCT_SECTION = '/bitrix/defa_cache/_js/',
        /** @var string IFRAME */
        IFRAME = '//www.kia.ru/upload/models/panarama/source/frame.html?pano_xml=%2F%2Fwww.kia.ru%2Fupload%2Fmodels%2Fpanarama%2F__xml%2F';

}

