<?php
declare(strict_types=1);

namespace Api\V1\Product;

use Bitrix\Main;
use Bitrix\Main\Entity;
use Bitrix\Main\Localization\Loc;
/**
 * ���������� ����������� �������
 *
 * @author  timofey.semenenko
 *
 * Interface InterfaceProduct
 *
 * @package Api\V1\Product
 */
interface InterfaceProduct
{
    /**
     * getData
     *
     * @param string $codeModel
     *
     * @param string $page
     *
     * @return array
     */
    public function getData(string $codeModel, string $page): array;

}