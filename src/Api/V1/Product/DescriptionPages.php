<?php
declare(strict_types=1);

namespace Api\V1\Product;

/**
 * @author  timofey.semenenko
 *
 * @api     ����������� ��������
 *
 * @version v1.0
 *
 * DescriptionPages
 *
 * Class DescriptionPages
 *
 * @package Api\V1\Product
 */
class DescriptionPages implements \Api\V1\Product\InterfaceProduct
{

    /**
     * �������� ������ ������ ����������� �������
     *
     * getData
     *
     * @param string $page
     *
     * @param string $models
     *
     * @return array
     */
    public function getData(string $models, string $page): array
    {
        /** @var array $arRes */
        $arRes = [];
        /** @var array $arSection */
        $arSection = [
            'SECTION_ID' => reset((\Kia\Entity\Product\PagesSectionTable::GetList([
                'filter' => [
                    '=IBLOCK_SECTION_ID' => reset((\Kia\Entity\Product\PagesSectionTable::GetList([
                        'filter' => ['=CODE' => $models],
                        'select' => ['ID']
                    ]))->fetchAll())['ID'],
                    '=CODE' => $page
                ],
                'select' => ['ID', 'UF_COLOR_TEMPLATE', 'UF_CTA_CHOOSE_BUTTON', 'UF_PRE_ORDER'],
            ]))->fetchAll())
        ];
        /** @var array $arData */
        $arData = \Kia\Entity\Product\PagesTable::GetList([
                'filter' => [
                    'IBLOCK_SECTION_ID' => $arSection['SECTION_ID']['ID'],
                    'ACTIVE' => 'Y'
                ],
                'select' => [
                    'ID',
                    'NAME',
                    'CODE',
                    'PREVIEW_TEXT',
                    'DETAIL_TEXT',
                    'PREVIEW_PICTURE',
                    'DETAIL_PICTURE',
                    'HEADER_DESKIMG',
                    'HEADER_MOBIMG',
                    'HEADER_LINK_VIDEO',
                    'HEADER_CHOOSE_POSITION',
                    'PRICE_TYPE_MODEL',
                    'PRICE_TYPE_BUTTON1',
                    'PRICE_TYPE_BUTTON2',
                    'PRICE_PERSENT_VALUE',
                    'PRICE_VALUE_MONTH',
                    'PRICE_DATA_OF_SALE',
                    'PRICE_HIDE',
                    'REVIEWS',
                    'ENGINE_MODIFICATION',
                    'ENGINE_COMPLECTATION',
                    'ENGINE_MAXCOUNT_TORQUE',
                    'ENGINE_MAXCOUNT_POWER',
                    'NEEDHELP_BUTTONS',
                    'TITLE_BUTTON',
                    'SAFETY_FILE',
                    'SAFETY_BUTTON',
                    'CONTENT_PARALLAX',
                    'CONTENT_PARALLAX_MOB',
                    'CONTENT_GALLERY',
                    'CUSTOM_TITLE_BUTTON',
                    'CUSTOM_URL_BUTTON',
                    'CUSTOM_CHOOSE_BUTTON',
                    'CUSTOM_IMAGE_FILE',
                    'ACTION_GALLERY',
                    'DEALERS_FILE',
                    'DEALERS_BUTTON',
                    'FOOTER_BUTTON',
                    'FOOTER_SHARE_BUTTON',
                    'FOOTER_LIKE_BUTTON',
                    'DEALERS_DEALER_VALUE',
                    'CHOOSE_BLOCK',
                    'HEADER_LOGO',
                    'SORT',
                ],
                'order' => ['SORT' => 'ASC'],
            ]
        );
        /**
         * @todo : ����� �������� ����������� ������� ���������������� ����� �� d7, ���������� �������*
         * @var array $arTheme
         */
        $arTheme = \CUserFieldEnum::GetList(
            [],
            ["ID" => (int)$arSection['SECTION_ID']['UF_COLOR_TEMPLATE']]
        )->fetch();
        /** @var array $arButtonsCta */
        $arButtonsCta = [];
        /** @var array $valueUfFields */
        foreach ($arSection['SECTION_ID']['~UF_CTA_CHOOSE_BUTTON'] as $key => $valueUfFields) {
            /**
             * @todo : ����� �������� ����������� ������� ���������������� ����� �� d7, ���������� �������*
             */
            $arUfFields[] = \CUserFieldEnum::GetList(
                [],
                ["ID" => (int)$valueUfFields]
            )->fetch();
            $arButtonsCta[] = $arUfFields[$key]['XML_ID'];
        }
        $arRes['theme'] = $arTheme['XML_ID'];
        $arRes['path'] = \Kia\Entity\Constants::PATH_FOR_PRODUCT_SECTION . $models . '/' . $page . '/';
        $arRes['page'] = $page;
        $arRes['model'] = $models;
        $arRes['buttonsCTA'] = $arButtonsCta;
        $arRes['preorder'] = (bool)$arSection['SECTION_ID']['~UF_PRE_ORDER'];
        unset($arTheme);
        unset($arUfFields);
        unset($arButtonsCta);
        /** @var array $arFields */
        $arFields = $arData->fetchAll();

        /** @var array $arMultiplyProp ������������� �������� */
        $arMultiplyProp = [];

        /** @var array $arResult �������������� ������ �� �������� �������� JSON */
        $arResult = [];

        /** @var array $arElement */
        foreach ($arFields as $arElement) {
            if (!empty($arElement['PRICE_TYPE_BUTTON2'])) {
                $arMultiplyProp[$arElement['ID']]['PRICE_TYPE_BUTTON2'][] = $arElement['PRICE_TYPE_BUTTON2'];
            }
            if (!empty($arElement['NEEDHELP_BUTTONS'])) {
                $arMultiplyProp[$arElement['ID']]['NEEDHELP_BUTTONS'][] = $arElement['NEEDHELP_BUTTONS'];
            }
            if (!empty($arElement['REVIEWS'])) {
                $arMultiplyProp[$arElement['ID']]['REVIEWS'][] = $arElement['REVIEWS'];
            }
            if (!empty($arElement['CONTENT_GALLERY'])) {
                $arMultiplyProp[$arElement['ID']]['CONTENT_GALLERY'][] = $arElement['CONTENT_GALLERY'];
            }
            if (!empty($arElement['ACTION_GALLERY'])) {
                $arMultiplyProp[$arElement['ID']]['ACTION_GALLERY'][] = $arElement['ACTION_GALLERY'];
            }
        }

        /** @var array $arElement */
        foreach ($arFields as $arElement) {
            if (!empty($arMultiplyProp[$arElement['ID']]['PRICE_TYPE_BUTTON2'])) {
                $arElement['~PRICE_TYPE_BUTTON2'] = $arMultiplyProp[$arElement['ID']]['PRICE_TYPE_BUTTON2'];
                $arElement['PRICE_TYPE_BUTTON2'] = $arMultiplyProp[$arElement['ID']]['PRICE_TYPE_BUTTON2'];
            }
            if (!empty($arMultiplyProp[$arElement['ID']]['NEEDHELP_BUTTONS'])) {
                $arElement['~NEEDHELP_BUTTONS'] = $arMultiplyProp[$arElement['ID']]['NEEDHELP_BUTTONS'];
                $arElement['NEEDHELP_BUTTONS'] = $arMultiplyProp[$arElement['ID']]['NEEDHELP_BUTTONS'];
            }
            if (!empty($arMultiplyProp[$arElement['ID']]['REVIEWS'])) {
                $arElement['~REVIEWS'] = $arMultiplyProp[$arElement['ID']]['REVIEWS'];
                $arElement['REVIEWS'] = $arMultiplyProp[$arElement['ID']]['REVIEWS'];
            }
            if (!empty($arMultiplyProp[$arElement['ID']]['CONTENT_GALLERY'])) {
                $arElement['~CONTENT_GALLERY'] = $arMultiplyProp[$arElement['ID']]['CONTENT_GALLERY'];
                $arElement['CONTENT_GALLERY'] = $arMultiplyProp[$arElement['ID']]['CONTENT_GALLERY'];
            }
            if (!empty($arMultiplyProp[$arElement['ID']]['ACTION_GALLERY'])) {
                $arElement['~ACTION_GALLERY'] = $arMultiplyProp[$arElement['ID']]['ACTION_GALLERY'];
                $arElement['ACTION_GALLERY'] = $arMultiplyProp[$arElement['ID']]['ACTION_GALLERY'];
            }

            $arResult[$arElement['ID']] = $arElement;
        }

        foreach ($arResult as $key => $value) {

            switch ($value['CHOOSE_BLOCK']) {
                //����
                /**
                 * @todo refactor!
                 */
                case 'headerMenuBlock':
                    $arRes['blocks'][] = [
                        'name' => 'headerMenuBlock',
                        'content' => [
                            [
                                'title' => '��������',
                                'link' => '/models/' . $models . '/desc/',
                                // 'targetBlank' => true,
                            ],
                            [
                                'title' => '����������� �����������',
                                'link' => '/models/' . $models . '/spec/',
                                // 'targetBlank' => true,
                            ],
                            [
                                'title' => '������������ � ����',
                                'link' => '/models/' . $models . '/options/',
                                //  'targetBlank' => true,
                            ],
                            [
                                'title' => '��������������',
                                'link' => '/models/' . $models . '/properties/',
                                // 'targetBlank' => true,
                            ],
                            [
                                'title' => '����-�����',
                                'link' => '/request/order_testdrive/?model=' . $models,
                                // 'targetBlank' => true,
                            ],
                        ],
                    ];
                    break;
                //�����
                case 'header':
                    $arRes['blocks'][] = [
                        'name' => 'headerBlock',
                        'content' => [
                            'theme' => $value['HEADER_CHOOSE_POSITION'],
                            'title' => $value['NAME'],
                            'logo' => $value['HEADER_LOGO']['SRC'],
                            'subtitle' => !empty($value['PREVIEW_TEXT']) ? html_entity_decode($value['PREVIEW_TEXT']) : null,
                            'deskImg' => $value['HEADER_DESKIMG']['SRC'],
                            'mobImg' => $value['HEADER_MOBIMG']['SRC'],
                            'urlVideo' => $value['HEADER_LINK_VIDEO'],
                        ],
                    ];
                    break;
                // ��������� � ��������� ������������
                case 'topPriceBlockStartToSales':
                    /**
                     * @todo �������� �����, ���, �������� ���������
                     * @var array $arPrice
                     */
                    /** @var array $arPrice */
                    $arPrice = $value['PRICE_TYPE_MODEL'];
                    /** @var array $arModelInfo */
                    $arModelInfo = \CDFAAutoEntityFactory::getModel()->getModelParams((int)$value['PRICE_TYPE_MODEL']);
                    /** @var array $arMod */
                    $arMod = \CDFAModificationsEntityManager::GetInstance()->getCached(
                        [],
                        ['ID' => $arModelInfo['MODIF_FOR_CHARACTERISTICS']],
                        ['TYPE'],
                        \Kia\Entity\Constants::CACHE_TIME
                    );
                    $arRes['blocks'][] = [
                        'name' => 'priceCreditOffer',
                        'content' => [
                            'showPrice' => ($value['PRICE_HIDE'] == 'Y') ? true : false,
                            'price' => \CDFAAutoEntityFactory::getModel()->getMinPrice((int)$value['PRICE_TYPE_MODEL']),
                            'year' => $arModelInfo['YEAR_FROM'],
                            'model' => $arModelInfo['MODEL_NAME'],
                            'nameEngine' => $arMod['~TYPE'],
                            'valuePercent' => $value['PRICE_PERSENT_VALUE'],
                            'valuePayment' => $value['PRICE_VALUE_MONTH'],

                        ],
                    ];
                    unset($arMod);
                    unset($arPrice);
                    unset($arModelInfo);
                    break;
                // ��������� "����� ������"
                case 'topPriceBlockWithCreditOffer':
                    /**
                     * @todo �������� �����, ���, �������� ���������
                     * @var array $arPrice
                     */
                    $arPrice = $value['PRICE_TYPE_MODEL'];
                    /** @var array $arModelInfo */
                    $arModelInfo = \CDFAAutoEntityFactory::getModel()->getModelParams((int)$value['PRICE_TYPE_MODEL']);
                    /** @var array $arMod */
                    $arMod = \CDFAModificationsEntityManager::GetInstance()->getCached(
                        [],
                        ['ID' => $arModelInfo['MODIF_FOR_CHARACTERISTICS']],
                        ['TYPE'],
                        \Kia\Entity\Constants::CACHE_TIME
                    );
                    $arRes['blocks'][] = [
                        'name' => 'priceStartSalepriceButton',
                        'content' => [
                            'showPrice' => ($value['PRICE_HIDE'] == 'Y') ? true : false,
                            'price' => \CDFAAutoEntityFactory::getModel()->getMinPrice((int)$value['PRICE_TYPE_MODEL']),
                            'year' => $arModelInfo['YEAR_FROM'],
                            'model' => $arModelInfo['MODEL_NAME'],
                            'nameEngine' => $arMod['~TYPE'],
                            'date' => str_replace('-', '.', $value['PRICE_DATA_OF_SALE']),
                            'buttons' => self::getButtonBlock($value['PRICE_TYPE_BUTTON1']),
                        ],
                    ];
                    unset($arMod);
                    unset($arPrice);
                    unset($arModelInfo);
                    break;
                // ��������� � ��������� ������������ � 1 �������
                case 'topPriceBlockWithOneButton':
                    /**
                     * @todo �������� �����, ���, �������� ���������
                     * @var array $arPrice
                     */
                    $arPrice = $value['PRICE_TYPE_MODEL'];
                    /** @var array $arModelInfo */
                    $arModelInfo = \CDFAAutoEntityFactory::getModel()->getModelParams((int)$value['PRICE_TYPE_MODEL']);
                    /** @var array $arMod */
                    $arMod = \CDFAModificationsEntityManager::GetInstance()->getCached(
                        [],
                        ['ID' => $arModelInfo['MODIF_FOR_CHARACTERISTICS']],
                        ['TYPE'],
                        \Kia\Entity\Constants::CACHE_TIME
                    );
                    $arRes['blocks'][] = [
                        'name' => 'priceCreditOfferAndButton',
                        'content' => [
                            'showPrice' => ($value['PRICE_HIDE'] == 'Y') ? true : false,
                            'price' => \CDFAAutoEntityFactory::getModel()->getMinPrice((int)$value['PRICE_TYPE_MODEL']),
                            'year' => $arModelInfo['YEAR_FROM'],
                            'model' => $arModelInfo['MODEL_NAME'],
                            'nameEngine' => $arMod['~TYPE'],
                            'valuePercent' => $value['PRICE_PERSENT_VALUE'],
                            'valuePayment' => $value['PRICE_VALUE_MONTH'],
                            'buttons' => self::getButtonBlock($value['PRICE_TYPE_BUTTON1']),

                        ],
                    ];
                    unset($arMod);
                    unset($arPrice);
                    unset($arModelInfo);
                    break;
                // ��������� � 2 ��������
                case 'topPriceBlockWithButtons':
                    /**
                     * @todo �������� �����, ���, �������� ���������
                     * @var array $arPrice
                     */
                    $arPrice = $value['PRICE_TYPE_MODEL'];
                    /** @var array $arModelInfo */
                    $arModelInfo = \CDFAAutoEntityFactory::getModel()->getModelParams((int)$value['PRICE_TYPE_MODEL']);
                    /** @var array $arMod */
                    $arMod = \CDFAModificationsEntityManager::GetInstance()->getCached(
                        [],
                        ['ID' => $arModelInfo['MODIF_FOR_CHARACTERISTICS']],
                        ['TYPE'],
                        \Kia\Entity\Constants::CACHE_TIME
                    );
                    $arRes['blocks'][] = [
                        'name' => 'priceTwoButton',
                        'content' => [
                            'showPrice' => ($value['PRICE_HIDE'] == 'Y') ? true : false,
                            'price' => \CDFAAutoEntityFactory::getModel()->getMinPrice((int)$value['PRICE_TYPE_MODEL']),
                            'year' => $arModelInfo['YEAR_FROM'],
                            'model' => $arModelInfo['MODEL_NAME'],
                            'nameEngine' => $arMod['~TYPE'],
                            'buttons' => self::getFullButtons($value['PRICE_TYPE_BUTTON2']),
                        ],
                    ];
                    unset($arMod);
                    unset($arPrice);
                    unset($arModelInfo);
                    break;
                // ������
                case 'showroom':
                    $arRes['blocks'][] = [
                        'name' => 'showroomBlock',
                        'content' => [
                            'title' => $value['NAME'],
                            'description' => !empty($value['PREVIEW_TEXT']) ? html_entity_decode($value['PREVIEW_TEXT']) : null,
                            'modelCode' => $value['CODE'],
                        ]];
                    break;
                // ���������� ���� "����������� ���������"
                case 'parallax':
                    $arRes['blocks'][] = [
                        'name' => 'parallaxBlock',
                        'content' => [
                            'deskImg' => $value['CONTENT_PARALLAX']['SRC'],
                            'mobImg' => $value['CONTENT_PARALLAX_MOB']['SRC'],
                        ]];
                    break;
                // ���� "���������"
                case 'textBlock':
                    $arRes['blocks'][] = [
                        'name' => 'textBlock',
                        'content' => [
                            'title' => $value['NAME'],
                            'text' => !empty($value['PREVIEW_TEXT']) ? html_entity_decode($value['PREVIEW_TEXT']) : null,
                            'buttons' => self::getButtonBlock($value['TITLE_BUTTON']),
                        ]];
                    break;
                // ����� ������
                case 'helpBlock':
                    $arRes['blocks'][] = [
                        'name' => 'helpBlock',
                        'content' => [
                            'title' => $value['NAME'],
                            'buttons' => self::getFullButtons($value['NEEDHELP_BUTTONS']),
                        ]];
                    break;
                // ���������
                case 'engines':
                    $arRes['blocks'][] = [
                        'name' => 'enginesBlock',
                        'content' => self::getEngine((int)$value['ENGINE_MODIFICATION']),
                    ];
                    break;
                // ������������
                case 'safety':
                    $arRes['blocks'][] = [
                        'name' => 'safetyBlock',
                        'content' => [
                            'text' => !empty($value['DETAIL_TEXT']) ? html_entity_decode($value['DETAIL_TEXT']) : null,
                            'smallText' => !empty($value['PREVIEW_TEXT']) ? html_entity_decode($value['PREVIEW_TEXT']) : null,
                            'img' => $value['SAFETY_FILE']['SRC'],
                            'buttons' => self::getButtonBlock($value['SAFETY_BUTTON']),
                        ]];
                    break;
                // ������
                case 'reviews':
                    /** @var array $arReviewValue */
                    $arReviewValue = [];
                    /** @var array $arReviews */
                    $arReviews = [];
                    /**
                     * @var array $keyReviews
                     * @var array $idReview
                     */
                    foreach ($value['REVIEWS'] as $keyReviews => $idReview) {
                        /** @var array $arButton */
                        $arReviews[] = reset(\Kia\Entity\Product\ReviewTable::GetList([
                            'select' => [
                                'NAME',
                                'REVIEWS_VIDEO',
                                'REVIEWS_AUTHOR',
                                'PREVIEW_TEXT',
                                'PREVIEW_PICTURE',
                                'DETAIL_PICTURE',
                            ],
                            'filter' => ['ID' => $idReview],
                            'order' => ['SORT' => 'ASC'],
                        ])->fetchAll());
                        /** @var array $arReviewValue */
                        $arReviewValue[] = [
                            "reviewText" => !empty($arReviews[$keyReviews]['PREVIEW_TEXT']) ? html_entity_decode($arReviews[$keyReviews]['PREVIEW_TEXT']) : null,
                            'reviewAuthor' => $arReviews[$keyReviews]['NAME'],
                            'reviewIcon' => $arReviews[$keyReviews]['PREVIEW_PICTURE']['SRC'],
                            'reviewVideo' => $arReviews[$keyReviews]['REVIEWS_VIDEO'],
                            'reviewImg' => $arReviews[$keyReviews]['DETAIL_PICTURE']['SRC'],
                        ];
                    }
                    $arRes['blocks'][] = [
                        'name' => 'reviewsBlock',
                        'content' => $arReviewValue,
                    ];
                    unset($arReviews);
                    unset($arReviewValue);
                    break;
                // �����
                case 'footerInfo':
                    $arRes['blocks'][] = [
                        'name' => 'footerInfoBlock',
                        'content' => [
                            'title' => $value['NAME'],
                            'disclaimer' => !empty($value['PREVIEW_TEXT']) ? html_entity_decode($value['PREVIEW_TEXT']) : null,
                            'shareBtn' => ($value['FOOTER_SHARE_BUTTON'] == 'Y') ? true : false,
                            'likeBtn' => ($value['FOOTER_LIKE_BUTTON'] == 'Y') ? true : false,
                            'buttons' => self::getButtonBlock((string)$value['FOOTER_BUTTON']),
                        ],
                    ];
                    break;
                // ���������� ���� "����������� + ��������"
                case 'textImgBlockReverse':
                    $arRes['blocks'][] = [
                        'name' => 'textImgReverseBlock',
                        'content' => [
                            'title' => $value['NAME'],
                            'image' => $value['PREVIEW_PICTURE']['SRC'],
                            'text' => !empty($value['PREVIEW_TEXT']) ? html_entity_decode($value['PREVIEW_TEXT']) : null,

                        ],
                    ];
                    break;
                // ���������� ���� "�������� + �����������"
                case 'textImgBlock':
                    $arRes['blocks'][] = [
                        'name' => 'textImgBlock',
                        'content' => [
                            'title' => $value['NAME'],
                            'text' => !empty($value['PREVIEW_TEXT']) ? html_entity_decode($value['PREVIEW_TEXT']) : null,
                            'image' => $value['PREVIEW_PICTURE']['SRC'],

                        ],
                    ];
                    break;
                // ���������� ���� "������� � ��������� � ����"
                case 'textImageGallery':
                    /** @var array $arGalleryValue */
                    $arGalleryValue = [];
                    /** @var array $arGallery */
                    $arGallery = [];
                    /**
                     * @var array $keyGallery
                     * @var array $idGallery
                     */
                    foreach ($value['CONTENT_GALLERY'] as $keyGallery => $idGallery) {
                        /** @var array $arGallery */
                        $arGallery[] = reset(\Kia\Entity\Product\GalleryTable::GetList([
                            'select' => [
                                'NAME',
                                'PREVIEW_TEXT',
                                'PREVIEW_PICTURE',
                                'DETAIL_PICTURE',
                                'GALLERY_LINK',
                                'GALLERY_BUTTON_LINK',
                                'GALLERY_BUTTON_TITLE',
                            ],
                            'filter' => ['ID' => $idGallery],
                            'order' => ['SORT' => 'ASC'],
                        ])->fetchAll());
                        /** @var array $arGalleryValue */
                        $arGalleryValue[] = [
                            "title" => $arGallery[$keyGallery]['NAME'],
                            'text' => !empty($arGallery[$keyGallery]['PREVIEW_TEXT']) ? html_entity_decode($arGallery[$keyGallery]['PREVIEW_TEXT']) : null,
                            'imgDesk' => $arGallery[$keyGallery]['PREVIEW_PICTURE']['SRC'],
                            'imgMob' => $arGallery[$keyGallery]['DETAIL_PICTURE']['SRC'],
                        ];
                    }
                    $arRes['blocks'][] = [
                        'name' => 'textImgGallery',
                        'content' => $arGalleryValue,
                    ];
                    unset($arGallery);
                    unset($arGalleryValue);
                    break;
                //��������� ����
                case 'dealerContentBlock':
                    break;
                // �����
                case 'saleBlock':
                    /** @var array $arPromoValue */
                    $arPromoValue = [];
                    /** @var array $arPromo */
                    $arPromo = [];
                    /**
                     * @var array $keyPromo
                     * @var array $idPromo
                     */
                    foreach ($value['ACTION_GALLERY'] as $keyPromo => $idPromo) {
                        $arPromo[] = reset(\Kia\Entity\Product\ActionTable::GetList([
                            'select' => [
                                'NAME',
                                'PREVIEW_TEXT',
                                'PROMO_IMAGE_MOB',
                                'PROMO_IMAGE_DESK',
                                'DETAIL_TEXT',
                                'PROMO_BUTTON_VALUE',
                                'PROMO_VIDEO_LINK',
                                'ACTIVE_TO',
                                'ACTIVE_FROM',
                            ],

                            'filter' => [
                                'ID' => $idPromo,
                                '>=ACTIVE_TO' => new \Bitrix\Main\Type\DateTime(),
                            ],
                            'order' => ['SORT' => 'ASC'],
                        ])->fetchAll());
                        /** @var array $arPromoValue */
                        $arPromoValue[] = [
                            "title" => $arPromo[$keyPromo]['NAME'],
                            'text' => !empty($arPromo[$keyPromo]['PREVIEW_TEXT']) ? html_entity_decode($arPromo[$keyPromo]['PREVIEW_TEXT']) : null,
                            'deskImg' => $arPromo[$keyPromo]['PROMO_IMAGE_DESK']['SRC'],
                            'mobImg' => $arPromo[$keyPromo]['PROMO_IMAGE_MOB']['SRC'],
                            'descriptor' => !empty($arPromo[$keyPromo]['DETAIL_TEXT']) ? html_entity_decode($arPromo[$keyPromo]['DETAIL_TEXT']) : null,
                            'buttons' => self::getButtonBlock((string)$arPromo[$keyPromo]['PROMO_BUTTON_VALUE']),
                            'timerDate' => $arPromo[$keyPromo]['ACTIVE_FROM'],
                            'timerEnd' => $arPromo[$keyPromo]['ACTIVE_TO'],
                            'urlVideo' => $arPromo[$keyPromo]['PROMO_VIDEO_LINK'],
                        ];
                    }
                    $arRes['blocks'][] = [
                        'name' => 'promoBlock',
                        'content' => $arPromoValue,
                    ];

                    unset($arPromo);
                    unset($arPromoValue);
                    break;
                // ��������� ����
                case
                'customBlock':
                    $arRes['blocks'][] = [
                        'name' => 'coustomBlock',
                        'content' => [
                            'html' => !empty($value['PREVIEW_TEXT']) ? html_entity_decode($value['PREVIEW_TEXT']) : null,
                        ],
                    ];
                    break;
            }
        }
        if ($arRes['blocks'] == []) {
            abort(404, 'Not found');
        }
        return $arRes;
    }

    /**
     * �������� ������ ��� ���������� �����
     *
     * getButtonsBlock
     *
     * @param string $valueId
     *
     * @return array
     */
    protected static function getButtonBlock(string $valueId): array
    {
        /** @var array $arButton */
        $arButton = reset(\Kia\Entity\Product\ButtonsTable::GetList([
            'select' => [
                'NAME',
                'CHOOSE_COLOR',
                'CHOOSE_LINK',
                'CHOOSE_TITLE',
                'CHOOSE_ICON',
                'CHOOSE_BUTTONS',
                'CHOOSE_CLASS_BUTTON',
                'CHOOSE_DOWNLOAD_VALUE',
                'CHOOSE_TARGET_BLANK_VALUE',
            ],
            'filter' => ['ID' => $valueId]
        ])->fetchAll());
        /** @var array $theme */
        $theme = reset(\Bitrix\Iblock\PropertyEnumerationTable::getList([
            'select' => ['XML_ID'],
            'filter' => ['ID' => $arButton['CHOOSE_COLOR']],
            'cache' => [
                'ttl' => \Kia\Entity\Constants::CACHE_TIME,
                'cache_joins' => true,
            ],
        ])->fetchAll());
        /** @var array $arButtonValue */
        $arButtonValue = [];
        if (!empty($arButton)) {
            $arButtonValue[] = [
                "text" => $arButton['NAME'],
                'link' => $arButton['CHOOSE_LINK'],
                'icon' => $arButton['CHOOSE_ICON'],
                'targetBlank' => ($arButton['CHOOSE_TARGET_BLANK_VALUE'] == 'Y') ? true : false,
                'download' => ($arButton['CHOOSE_DOWNLOAD_VALUE'] == 'Y') ? true : false,
                'theme' => $theme['XML_ID'],
                'class' => $arButton['CHOOSE_CLASS_BUTTON']
            ];
        }
        unset($theme);
        unset($valueId);
        unset($arButton);
        return $arButtonValue;
    }

    /**
     * �������� ��� ������ ��� ���������� �����
     *
     * getFullButtons
     *
     * @param array $valueId
     *
     * @return array
     */
    protected static function getFullButtons(array $valueId): array
    {
        /** @var array $arButtonValue */
        $arButtonValue = [];
        /** @var array $arButton */
        $arButton = [];
        /**
         * @var array $keyButtons
         * @var array $idButtons
         */
        foreach ($valueId as $keyButtons => $idButtons) {
            /** @var array $arButton */
            $arButton[] = reset(\Kia\Entity\Product\ButtonsTable::GetList([
                'select' => [
                    'NAME',
                    'CHOOSE_COLOR',
                    'CHOOSE_LINK',
                    'CHOOSE_TITLE',
                    'CHOOSE_ICON',
                    'CHOOSE_BUTTONS',
                    'CHOOSE_CLASS_BUTTON',
                    'CHOOSE_DOWNLOAD_VALUE',
                    'CHOOSE_TARGET_BLANK_VALUE'
                ],
                'filter' => ['ID' => $idButtons]
            ])->fetchAll());
            /** @var array $arButtonValue */
            $arButtonValue[] = [
                "text" => $arButton[$keyButtons]['NAME'],
                'link' => $arButton[$keyButtons]['CHOOSE_LINK'],
                'icon' => $arButton[$keyButtons]['CHOOSE_ICON'],
                'targetBlank' => ($arButton[$keyButtons]['CHOOSE_TARGET_BLANK_VALUE'] == 'Y') ? true : false,
                'download' => ($arButton[$keyButtons]['CHOOSE_DOWNLOAD_VALUE'] == 'Y') ? true : false,
                'theme' => reset(\Bitrix\Iblock\PropertyEnumerationTable::getList([
                    'select' => ['XML_ID'],
                    'filter' => ['ID' => $arButton[$keyButtons]['CHOOSE_COLOR']],
                    'cache' => [
                        'ttl' => \Kia\Entity\Constants::CACHE_TIME,
                        'cache_joins' => true,
                    ],
                ])->fetchAll())['XML_ID'],
                'class' => $arButton[$keyButtons]['CHOOSE_CLASS_BUTTON']
            ];
        }
        unset($arButton);
        return $arButtonValue;

    }

    /**
     * @todo ����������� ����� ������ � ������ ������� ����� ����������, ����������!
     *
     * ��������� ���� ����������.
     *
     * @param string $modelId id ������������� ������.
     *
     * @return array ���� ���. ��������� id �������� ������ => �������� ������.
     */
    protected static function getEngine(int $modelId): array
    {
        /** @var array $arEngine */
        $arEngine = [];
        /** @var array $arMod */
        $arMod = [];
        /** @var array $arModInfo */
        $arModInfo = \CDFAAutoEntityFactory::getModel()->getModelParams($modelId, 'MODIF_INFO');
        /**
         * @var array $key
         * @var array $value
         */
        foreach ($arModInfo as $key => $value) {
            /**
             * @todo ������� ��-�� �������� � ������� api ������ ������ ��������� ���������� �� ������ ������; refactor!
             */
            if (is_int($key)) {
                $arMod[$value['~TYPE']] = $value;
            }

        }
        /**
         * @todo ������� ���� �� ������ ������ ������ ��������� � �.�!
         *
         * @var array $keyMod
         *
         * @var array $valueMod
         */
        foreach ($arMod as $keyMod => $valueMod) {
            if (!empty($keyMod)) {
                /** @var string $consumption */
                $test[] = $valueMod;
                $consumption = \CDFAAutoEntityFactory::getModel()->getFuelConsumption($modelId, $valueMod['ID']);
                $arEngine[] = [
                    'engineName' => $valueMod['~TYPE'],
                    'power' => str_replace(',', '.', substr($valueMod['~POWER'], 0, strrpos($valueMod['~POWER'], '('))),
                    'powerPerMin' => (preg_match('#\((.*?)\)#', $valueMod['~POWER'], $match)) ? $match[1] : '0',
                    'powerTorque' => str_replace(',', '.', substr($valueMod['~MOMENT'], 0, strrpos($valueMod['~MOMENT'], '('))),
                    'powerTorquePerMin' => (preg_match('#\((.*?)\)#', $valueMod['MOMENT'], $match)) ? $match[1] : '0',
                    'consumption' => $valueMod['SMESH'],
                    /**
                     * ���� ������� ���������������, ����� ������ ������� ���� ������ ������ ���� ����� ������������!
                     * consumption' => $consumption['COMBI']
                     */
                    'acceleration' => $valueMod['RAZGON0'],
                ];
            }
        }
        unset($arMod);
        unset($arModInfo);
        unset($consumption);
        return (empty($arEngine)) ? [] : $arEngine;
    }

}