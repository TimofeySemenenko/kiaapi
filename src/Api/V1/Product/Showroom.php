<?php
declare(strict_types=1);

namespace Api\V1\Product;

/**
 * @author  timofey.semenenko
 *
 * @api     ������ �������
 *
 * @version v1.0
 *
 * Class Showroom
 *
 * @package Api\V1\Product
 */
class Showroom implements \Api\V1\Product\InterfaceProduct
{
    /**
     *  ��������� ���-���� ��� ������
     *
     * version 1.0
     *
     * @param string      $codeModel
     *
     * @param string|null $page
     *
     * @return array
     */
    public function getData(string $codeModel, string $page = null): array
    {
        if (!is_null($codeModel)) {
            /** @var array $arRes */
            $arRes = [];
            /** @var array $arImages */
            $arImages = [];
            /**
             * @todo : ����������� ����� api ��� �������� "���� �������"
             * @var array $arColors
             */
            $arElements = \CDFAAutoEntityFactory::getColors()->getCached(
                ['SORT' => "ASC"],
                ['SECTION_CODE' => $codeModel],
                [
                    'COLOR',
                    'INVERSE',
                    'CONF_IMAGE',
                    'MODEL_ID',
                    'COLOR_IMAGE',
                    'START_IMAGE',
                    'COLOR_IMAGE',
                    'IS_METALLIC',
                    'ADDITIONAL_PRICE',
                    'SHOW_ROOM_ROTATOR',
                    'SWF_FILE',
                    'COLOR_SECOND'
                ]
            );
            /**
             * @var string     $key
             * @var string|int $value
             */
            foreach ($arElements as $key => $value) {
                /**
                 * @todo :����������� ����� ����� ��� ��������� ���.����;
                 * @var int $getModelPrice
                 */
                $getModelPrice = \CDFAAutoModelEntityManager::GetInstance()->getMinPrice(
                    $value['MODEL_ID']
                );
                /**
                 * @var string $keyImages
                 * @var string $itemImages
                 */
                foreach ($value['SHOW_ROOM_ROTATOR'] as $keyImages => $itemImages) {
                    /** @var array $arPropImagesInfo */
                    $arPropImagesInfo = \CFile::GetFileArray((int)$itemImages);
                    $arImages[$key][] = $arPropImagesInfo['SRC'];
                }
                if (!empty($arImages[$key])) {
                    $arRes['exterior']['colors'][$key] = [
                        'title' => $value['NAME'],
                        'secondHex' => $value['COLOR_SECOND'],
                        'hex' => $value['COLOR'],
                        'price' => (int)$value['ADDITIONAL_PRICE'],
                        'images' => $arImages[$key],
                    ];
                }
                $arRes['interior'] = [
                    'iframeUrl' => \Kia\Entity\Constants::IFRAME . $codeModel . '.xml'
                ];
            }
            unset($getModelPrice);
            unset($arElements);
            unset($itemImages);
            unset($value);

        } else {
            abort(400, 'Model code is empty!');
        }

        if ($arRes == []) {
            abort(404, 'Not found');
        }

        return $arRes;
    }

}