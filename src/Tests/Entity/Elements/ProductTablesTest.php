<?php
declare(strict_types=1);

namespace Tests\Entity\Elements;

use Bitrix\Main\Application;
use \Tests\BitrixTestCase;

/**
 * @author  timofey.semenenko
 *
 * ��������� Create Read Delete ��������� ��� �������� PagesTable
 *
 * Class ProductTablesTest
 *
 * @package Tests\Entity\Elements
 */
class ProductTablesTest extends BitrixTestCase
{
    use \Kia\Traits\Property\AllProperty;
    /**
     * @var int
     */
    protected $idElement;
    /**
     * @var BooksRepository
     */
    protected $result;
    /**
     *  \CIBlockElement
     */
    protected $objElement;

    /**
     * @inheritdoc
     */
    public function setUp()
    {
        parent::setUp();
    }

    /**
     * testAddElement
     */
    public function testAddElement()
    {

        /** @var array $arProp */
        $arProp = [];
        /** @var array $arDataForAdd */
        foreach (array_flip($this->getAllProp(\Kia\Entity\Constants::IBLOCK_ID_TEST_CASE)) as $key => $value) {
            /** @var array $arResultChoose */
            $arResType = reset((\Bitrix\Iblock\PropertyTable::getList([
                'select' => ['PROPERTY_TYPE'],
                'filter' => [
                    'IBLOCK_ID' => \Kia\Entity\Constants::IBLOCK_ID_TEST_CASE,
                    'CODE' => $key,
                ],
                'cache' => [
                    'ttl' => \Kia\Entity\Constants::CACHE_TIME,
                    'cache_joins' => true,
                ],

            ]))->fetchAll());
            /**
             *
             * @todo defa-orm ������� �� ����������� �������, ��-�� ����� �� ����������� ����� ��� ���� �������� ������������ �����
             *
             */
            switch ($arResType['PROPERTY_TYPE']) {
                case  'L':
                    $arProp[$key] = (int)$this->faker->randomNumber;
                    break;
                case  'S':
                    $arProp[$key] = new \Bitrix\Main\Type\DateTime();
                    break;
                case  'N':
                    $arProp[$key] = (int)$this->faker->randomNumber;
                    break;
                case  'F':
                    $arProp[$key] = (int)$this->faker->randomNumber;
                    break;
                case  'G':
                    $arProp[$key] = (int)$this->faker->randomNumber;
                    break;
                case  'E':
                    $arProp[$key] = (int)$this->faker->randomNumber;
                    break;
                case  'S:DateTime':
                    $arProp[$key] = new \Bitrix\Main\Type\DateTime();
                    break;
                case  'S:HTML':
                    $arProp[$key] = $this->faker->word;
                    break;
                case  'S:ElementXmlID':
                    $arProp[$key] = (int)$this->faker->randomNumber;
                    break;
                case  'S:map_google':
                    $arProp[$key] = (int)$this->faker->randomNumber;
                    break;
                case  'S:map_yandex':
                    $arProp[$key] = (int)$this->faker->randomNumber;
                    break;
            }
        }
        $arProp['TEST_CASE'] = 'Y';
        /** @var array $arDataForAdd */
        $arDataForAdd = [
            'IBLOCK_ID' => \Kia\Entity\Constants::IBLOCK_ID_TEST_CASE,
            'NAME' => $this->faker->word,
            'ACTIVE' => 'N',
            'PROPERTY_VALUES' => $arProp,
        ];
        $this->objElement = new \CIBlockElement;
        $this->idElement = $this->objElement->Add($arDataForAdd);
        unset($arResType);
    }

    /**
     * @depends testAddElement
     *
     * @return array
     */
    public function testGetListElements()
    {

        return (\Kia\Entity\Product\PagesTable::GetList([
                'filter' => [
                    'ID' => (int)$this->idElement,
                    'ACTIVE' => 'N',
                    'TEST_CASE' => 'Y',

                ],
                'select' => $this->getAllProp(\Kia\Entity\Constants::IBLOCK_ID_TEST_CASE),
                'order' => ['SORT' => 'ASC'],
            ]
        ))->fetchAll();
    }

    /**
     * @depends testGetListElements
     *
     * @return bool
     */
    public function testDeleteElements()
    {
        $arDel = (\Kia\Entity\Product\PagesTable::GetList([
                'filter' => [
                    '=TEST_CASE' => 'Y',
                ],
                'select' => ['ID', 'NAME', 'TEST_CASE'],
            ]
        ))->fetchAll();
        foreach ($arDel as $value) {
            \CIBlockElement::Delete($value['ID']);
        }
        unset($arDel);
    }

    /**
     * @inheritdoc
     */
    public function tearDown()
    {
        parent::tearDown();
        $this->objElement = null;
        $this->result = null;
        $this->idElement = null;
    }
}