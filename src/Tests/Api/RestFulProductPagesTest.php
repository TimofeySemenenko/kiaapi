<?php
declare(strict_types=1);

namespace Tests\Api;

use \GuzzleHttp;
use \Tests\BitrixTestCase;
use Bitrix\Main\Application;

/**
 * @author  timofey.semenenko
 *
 * ��������� ����������� Api ��� ���� �������� ����������� �������
 *
 * Class RestFulProductPagesTest
 *
 * @package Tests\Api
 */
class RestFulProductPagesTest extends BitrixTestCase
{
    /**
     * @var GuzzleHttp\Client $http
     */
    private $http;
    /**
     * @var array $arModels
     */
    protected $arModels = [];

    /**
     * @inheritdoc
     */
    public function setUp()
    {

        parent::setUp();
        $this->http = new GuzzleHttp\Client([
            'base_uri' => 'https://dev01.kia.dev.defa.ru/restfull/v1/product/showroom',
            'verify' => false,
            'curl' => [
                CURLOPT_SSL_VERIFYPEER => false,
                CURLOPT_SSL_VERIFYHOST => false
            ],
        ]);
    }

    /**
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function testGet()
    {
        /** @var guzzleHttp\Client $response */
        $response = $this->http->request(
            'GET',
            '?modelCode=picanto'
        );

        $this->assertEquals(200, $response->getStatusCode());
        /** @var guzzleHttp\Client $contentType */
        $contentType = $response->getHeaders()["Content-Type"][0];
        $this->assertEquals("application/json", $contentType);
        /** @var guzzleHttp\Client $userAgent */
        $userAgent = json_decode($response->getBody())->{"user-agent"};
        $this->assertRegexp('/Guzzle/', $userAgent);
    }

    /**
     * @inheritdoc
     */
    public function tearDown()
    {
        parent::tearDown();
        $this->http = null;
    }
}