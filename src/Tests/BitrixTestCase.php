<?php
declare(strict_types=1);

namespace Tests;
/**
 * @todo refactor ������������ ������������� ������� ����� ������
 */
require_once 'bootstrap.php';

/**
 * Class BitrixTestCase
 *
 * @package Tests
 */
abstract class BitrixTestCase extends \PHPUnit_Framework_TestCase
{
    /**
     * @var bool
     */
    protected $backupGlobals = false;

    /**
     * @var \Generator
     */
    protected $faker;

    /**
     * ���� ����� phpUnit �������� ����� �������� �������� �����
     *
     * setUp
     *
     * @inheritdoc
     */
    public function setUp()
    {
        //������������� ����
        initBitrixCore();
        // �������� ���������� Faker, ������� ����� ��������� ��������� ������
        $this->faker = \Faker\Factory::create();
    }

    /**
     * ���� ����� phpUnit �������� ����� ���������� �������� �����
     *
     * tearDown
     *
     * @inheritdoc
     */
    public function tearDown()
    {
        // ��� ����� ������ Mockery �� ����� ��������
        \Mockery::close();
    }
}