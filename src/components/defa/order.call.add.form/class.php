<?php

namespace Defa\Components;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

use Bitrix\Main\Loader,
    Bitrix\Iblock\ElementTable,
    Bitrix\Iblock\PropertyTable,
    Bitrix\Iblock\PropertyEnumerationTable,
    Bitrix\Main\Application,
    Bitrix\Main\SystemException,
    Bitrix\Main\Data\Cache,
    Bitrix\Main\Localization\Loc,
    Defa\Hightloadblock\Request\InputSource,
    Bitrix\Main\Mail\Event;

Loc::loadMessages(__FILE__);
Loader::includeModule('iblock');

class OrderCallAddForm extends \CBitrixComponent
{
    protected
        $request = array(),
        $arErrors = array(),
        $nIBlockId,
        $arIBlockProps = array(),
        $arIBlockPropsSort = array(),
        $sFormName = 'order_call',
        $sFormId = 'form_order_call_',
        $cacheTime = 0,
        $requiredFields = array(),
        $bUseAgreement = false,
        $arFormData = array(),
        $sDisclaimerCode = 'order_call_form',
        $sDealersScriptsFormCode = 'form-callback',
        $arGATrackParams = array(),
        $sGATrackClass = 'gatrack',
        $bShowPopupButton = false,
        $sEmailEventType = 'ORDER_CALL',
        $sSiteId = '',
        $sEmailTopicName = '����� ������';


    public function onPrepareComponentParams($arParams)
    {
        if ($arParams['IBLOCK_ID'] > 0) {
            $this->nIBlockId = (int)$arParams['IBLOCK_ID'];
        } else {
            $this->arErrors[] = '�� ������ ID ���������';
        }

        if ($arParams['CACHE_TIME'] > 0) {
            $this->cacheTime = (int)$arParams['CACHE_TIME'];
        }

        if (is_array($arParams['REQUIRED_FIELDS']) && !empty($arParams['REQUIRED_FIELDS'])) {
            foreach ($arParams['REQUIRED_FIELDS'] as $code) {
                $this->requiredFields[] = $code;
            }
        }

        if (!empty($arParams['FORM_NAME'])) {
            $this->sFormName = $arParams['FORM_NAME'];
        }

        if ($arParams['USE_AGREEMENT'] == 'Y') {
            $this->bUseAgreement = true;
        }

        if (!empty($arParams['DISCLAIMER_CODE'])) {
            $this->sDisclaimerCode = $arParams['DISCLAIMER_CODE'];
        }

        if (!empty($arParams['CLASS_CONTAINER'])) {
            $this->sDealersScriptsFormCode = $arParams['CLASS_CONTAINER'];
        }

        if (is_array($arParams['GATRACK_EVENTS']) && !empty($arParams['GATRACK_EVENTS'])) {
            $this->arGATrackParams = $arParams['GATRACK_EVENTS'];
        }

        if (is_array($arParams['SORT_FIELDS']) && !empty($arParams['SORT_FIELDS'])) {
            $this->arIBlockPropsSort =$arParams['SORT_FIELDS'];
            asort($this->arIBlockPropsSort);
        }

        if ($arParams['SHOW_POPUP_BUTTON'] == 'Y') {
            $this->bShowPopupButton = true;
        }

        $arParams['INIT_DEALERS_MAP'] = ($GLOBALS['DEALER_POPUP_MAP']['INITED'] === true) ? false : true;

        if (is_array($arParams['DEALER_MAP_PARAMS']) && !empty($arParams['DEALER_MAP_PARAMS'])) {
            $arParams['DEALER_MAP_PARAMS'] = $arParams['DEALER_MAP_PARAMS'];
        } elseif (is_array($GLOBALS['DEALER_POPUP_MAP']['PARAMS']) && !empty($GLOBALS['DEALER_POPUP_MAP']['PARAMS'])) {
            $arParams['DEALER_MAP_PARAMS'] = $GLOBALS['DEALER_MAP_PARAMS'];
        } else {
            $arParams['DEALER_MAP_PARAMS'] = array();
        }

        $this->sFormId .= md5(serialize(array_merge($this->arParams)));
        $this->sSiteId = SITE_ID;

        return $arParams;
    }

    public function executeComponent()
    {
        $this->request = Application::getInstance()->getContext()->getRequest();

        $this->setResultData();

        if ($this->request->getRequestMethod() == 'POST' && $this->request->getPost('AJAX_' . $this->sFormId) == 'Y') {
            $this->processForm();
        }

        $this->arResult['ERRORS'] = $this->arErrors;

        $this->includeComponentTemplate();
    }

    /**
     * ������������ $arResult
     */
    protected function setResultData()
    {
        $this->arResult['FORM'] = array(
            'ID' => $this->sFormId,
            'NAME' => $this->sFormName
        );

        $this->arResult['FIELDS'] = $this->getFields();
        $this->setVals();
        $this->arResult['JS_VALIDATE_FORM_DATA'] = $this->getJsValidateFormData();
        $this->arResult['AGREEMENT'] = array(
            'PREVIEW' => $this->getAgreementDescription(),
            'DETAIL' => $this->getAgreementText()
        );
        $this->arResult['CLASS_CONTAINER'] = $this->sDealersScriptsFormCode;
    }

    /**
     * ������ ����� ��� �����
     * @return array
     */
    protected function getFields()
    {
        $result = array();
        foreach ($this->getIBlockProps() as $arProperty) {
            $result[$arProperty['CODE']] = array(
                'NAME' => $arProperty['NAME'],
                'CODE' => $arProperty['CODE'],
                'FIELD_NAME' => strtolower($arProperty['CODE']),
                'HTML_FIELD_NAME' => $this->sFormName . '[' . strtolower($arProperty['CODE']) . ']',
                'HTML_FIELD_ID' => $this->sFormId . '_' . strtolower($arProperty['CODE']),
                'TYPE' => $this->getFieldType($arProperty),
                'VALUES' => $this->getFieldValues($arProperty),
                'REQUIRED' => $this->getFieldRequired($arProperty),
                'ERROR_MSG' => Loc::getMessage('ERROR_FIELD_EMPTY') . ' ' . $arProperty['NAME']
            );
        }

        $result = $this->sortFields($result);

        if ($this->bUseAgreement) {
            $result['AGREEMENT'] = array(
                'NAME' => Loc::getMessage('AGREEMENT_NAME'),
                'CODE' => 'AGREEMENT',
                'FIELD_NAME' => 'agreement',
                'HTML_FIELD_NAME' => $this->sFormName . '[agreement]',
                'HTML_FIELD_ID' => $this->sFormId . '_agreement',
                'REQUIRED' => 'Y',
                'ERROR_MSG' => Loc::getMessage('ERROR_FIELD_EMPTY') . ' ' . Loc::getMessage('AGREEMENT_NAME')
            );
        }

        return $result;
    }

    /**
     * ��������� �������� ����� �� �����
     */
    protected function setVals()
    {
        $this->arFormData = $this->request->getPost($this->sFormName);

        foreach ($this->arResult['FIELDS'] as $k => $arField) {
            $this->arResult['FIELDS'][$k]['VALUE'] = $this->arFormData[$arField['FIELD_NAME']];
        }
    }

    /**
     * ��������� ������ �����
     * ���������� �������� ��
     */
    protected function processForm()
    {
        if (!$this->checkFields()) {
            return false;
        }

        $arFields = array(
            'ACTIVE' => 'Y',
            'IBLOCK_ID' => $this->nIBlockId,
            'NAME' => $this->arFormData['phone'],
        );

        foreach ($this->arResult['FIELDS'] as $arField) {
            $arFields['PROPERTY_VALUES'][$arField['CODE']] = $this->arFormData[$arField['FIELD_NAME']];
        }

        $el = new \CIBlockElement();
        $res = $el->Add($arFields);

        if (!$res) {
            $this->arErrors[] = $el->LAST_ERROR;
        } else {
            $this->arResult['SUCCESS'] = 'Y';
            $this->arResult['DEALER_SUCCESS_SCRIPTS'] = \CDFADealerEntityManagerFactory::createDealer()->showSendCounters($this->sDealersScriptsFormCode);
            $this->sendSourceStatisticData($res);
            $this->sendEmail();
        }
    }

    /**
     * �������� ������������� �����
     * @return bool
     */
    protected function checkFields()
    {
        foreach ($this->arResult['FIELDS'] as $k => $arField) {
            if (!$arField['REQUIRED']) {
                continue;
            }

            switch ($arField['TYPE']) {
                default:
                    if (empty($arField['VALUE'])) {
                        $this->arErrors[$arField['FIELD_NAME']] = $arField['ERROR_MSG'];
                    }
            }
        }

        return empty($this->arErrors);
    }

    /**
     * ������� ������� ���������
     * @return array
     */
    protected function getIBlockProps()
    {
        $result = array();

        try {
            $cache = Cache::createInstance();
            $cacheId = "#order_call_iblock_props_" . $this->nIBlockId;
            if ($cache->initCache($this->cacheTime, $cacheId)) {
                $result = $cache->getVars();
            } elseif ($cache->startDataCache()) {
                $result = PropertyTable::getList(
                    array(
                        'filter' => array(
                            'IBLOCK_ID' => $this->nIBlockId,
                            'ACTIVE' => 'Y'
                        ),
                        'order' => array('SORT' => 'ASC')
                    )
                )->fetchAll();
                $cache->endDataCache($result);
            }
        } catch (SystemException $e) {
            $this->arErrors[] = $e->getMessage();
        }

        return $result;
    }

    /**
     * ���������� ��� �������� ����� ��������
     * ��������� ��� ���������� ����� XML_ID ��������
     * @param array $arProperty
     * @return string
     */
    protected function getFieldType($arProperty = array())
    {
        $result = 'text';
        switch ($arProperty['PROPERTY_TYPE']) {
            case 'S' :
                if ($arProperty['CODE'] == 'PHONE' || $arProperty['USER_TYPE'] == 'DefaPhone') {
                    $result = 'tel';
                }
                break;
            case 'L' :
                if ($arProperty['LIST_TYPE'] == 'C') {
                    $result = 'checkbox';
                } elseif ($arProperty['LIST_TYPE'] == 'L') {
                    $result = 'select';
                }
                break;
            case 'E' :
                if (!empty($arProperty['XML_ID'])) {
                    $result = $arProperty['XML_ID'];
                } elseif ($arProperty['LIST_TYPE'] == 'C') {
                    $result = 'checkbox';
                } elseif ($arProperty['LIST_TYPE'] == 'L') {
                    $result = 'select';
                }
                break;
        }

        return $result;
    }

    /**
     * ��������� �������� ��� ����
     * @param array $arProperty
     * @return array
     */
    protected function getFieldValues($arProperty = array())
    {
        $result = array();

        if (self::checkFieldHasValues($arProperty)) {
            $cache = Cache::createInstance();
            $cacheId = "#order_call_prop_values_" . strtolower($arProperty['CODE']);
            if ($cache->initCache($this->cacheTime, $cacheId)) {
                $result = $cache->getVars();
            } elseif ($cache->startDataCache()) {
                switch ($arProperty['PROPERTY_TYPE']) {
                    case 'L':
                        $result = $this->getFieldEnumValues($arProperty);
                        break;
                    case 'E':
                        // TODO: ������� � �����
                        $rs = ElementTable::getList(
                            array(
                                'filter' => array(
                                    'IBLOCK_ID' => $arProperty['LINK_IBLOCK_ID'],
                                    'ACTIVE' => 'Y'
                                ),
                                'select' => array(
                                    'ID',
                                    'NAME'
                                )
                            )
                        );

                        while ($ar = $rs->fetch()) {
                            $result[$ar['ID']] = $ar['NAME'];
                        }
                        break;
                }
                $cache->endDataCache($result);
            }
        }

        return $result;
    }

    /**
     * �������� ����� �� ���� ��������,
     * ������������ ��� ������� ������� � ������ � ������
     *
     * @param array $arProperty
     * @return bool
     */
    protected static function checkFieldHasValues($arProperty = array())
    {
        if (!($arProperty['PROPERTY_TYPE'] == 'L' || $arProperty['PROPERTY_TYPE'] == 'E') ||
            $arProperty['XML_ID'] == 'hidden' || $arProperty['XML_ID'] == 'dealer_map') {
            return false;
        }

        return true;
    }

    /**
     * ������� �������� �������� ���� ������
     * @param array $arProperty
     * @return array
     */
    protected function getFieldEnumValues($arProperty = array())
    {
        $result = array();

        $rs = PropertyEnumerationTable::getList(
            array(
                'filter' => array(
                    'PROPERTY_ID' => $arProperty['ID']
                ),
                'select' => array(
                    'ID',
                    'VALUE'
                ),
                'order' => array('SORT' => 'ASC')
            )
        );

        while ($ar = $rs->fetch()) {
            $result[$ar['ID']] = $ar['VALUE'];
        }

        return $result;
    }

    /**
     * �������� ���������������� ���������� ����
     * @param array $arProperty
     * @return string
     */
    protected function getFieldRequired($arProperty = array())
    {
        $result = 'N';
        if (in_array($arProperty['CODE'], $this->requiredFields)) {
            $result = 'Y';
        }

        return $result;
    }

    /**
     * ������ ��� ���������� �����
     * @return array
     */
    protected function getJsValidateFormData()
    {
        return array(
            'REQUIRED' => $this->getJsValidateFormRules(),
            'MESSAGES' => $this->getJsValidateFormMessages()
        );
    }

    /**
     * ����, ����������� ��� ���������� ��� ����������
     * @return array
     */
    protected function getJsValidateFormRules()
    {
        $result = array();
        foreach ($this->arResult['FIELDS'] as $arField) {
            if ($arField['REQUIRED'] == 'Y') {
                $result[$arField['CODE']] = $arField['HTML_FIELD_NAME'];
            }
        }

        return $result;
    }

    /**
     * ��������� ��� ����������
     * @return array
     */
    protected function getJsValidateFormMessages()
    {
        $result = array();
        foreach ($this->getJsValidateFormRules() as $code => $id) {
            $result[$id] = $this->arResult['FIELDS'][$code]['NAME'];
        }
        return $result;
    }

    /**
     * ���������� ����� ����������
     * @return string
     */
    protected function getAgreementText()
    {
        return \CDFAAutoEntityFactory::getDisclaimer()->getDisclaimerByCode($this->sDisclaimerCode);
    }

    /**
     * ���������� ����� �������� ����������
     * @return string
     */
    protected function getAgreementDescription()
    {
        return \CDFAAutoEntityFactory::getDisclaimer()->getDisclaimerPreviewByCode($this->sDisclaimerCode);
    }

    /**
     * ������ ���������� � highload-���� InputSourceOnWebsite
     * @param $id
     */
    protected function sendSourceStatisticData($id)
    {
        if ($id > 0) {
            $arSetFields = array();
            $arSetFields['UF_REQUEST_ID'] = $id;
            $arSetFields['UF_DATE'] = ConvertTimeStamp(time(), "FULL");
            $arSetFields['UF_REQUEST_TYPE'] = InputSource::getRequestType($this->nIBlockId);

            InputSource::setSource($arSetFields);
        }
    }

    /**
     * ����� ��������� �������� ��� �������� �������� �����
     */
    public function showDealerSuccessFormScripts()
    {
        \CDFADealerEntityManagerFactory::createDealer()->showSendCounters($this->sDealersScriptsFormCode);
    }

    /**
     * ������������ ������ � ���������� ��� GATrack
     *
     * @param $sCode
     * @return string
     */
    public function getGATrackByCode($sCode)
    {
        $result = '';
        if ($this->isGATrackableByCode($sCode)) {
            foreach ($this->arGATrackParams[$sCode] as $attr => $value) {
                $result .= ' ' . $attr . '="' . $value . '"';
            }
        }
        return $result;
    }

    /**
     * �������� ���� �� GATrack ���������
     *
     * @param $sCode
     * @return bool
     */
    private function isGATrackableByCode($sCode)
    {
        if (key_exists($sCode, $this->arGATrackParams)) {
            return true;
        }
        return false;
    }

    /**
     * ����� gatrack ������, ���� ������ ���������
     *
     * @param $sCode
     * @return string
     */
    public function showGATrackClassByCode($sCode)
    {
        $result = '';
        if ($this->isGATrackableByCode($sCode)) {
            $result = $this->sGATrackClass;
        }
        return $result;
    }

    /**
     * ���������� ����� �����
     * @param $arFields
     * @return array
     */
    protected function sortFields($arFields)
    {
        return array_merge($this->arIBlockPropsSort, $arFields);
    }

    /**
     * ���������� �� ������ ������ ������
     * @return bool
     */
    public function bShowButton()
    {
        return $this->bShowPopupButton;
    }

    /**
     * �������� ������ ������
     */
    public function sendEmail()
    {
        // ����������� ������ ��� ��������� �������
        $arEventData = array(
            'EVENT_NAME' => $this->sEmailEventType,
            'LID' => $this->sSiteId,
            'C_FIELDS' => array(
                'DATE' => date('d.m.Y H:i:s'),
                'TOPIC' => $this->sEmailTopicName,
            ),
        );

        // ������ ��������������� ������������
        if (!empty($arUserData = $this->getUserData())) {
            $arEventData['C_FIELDS']['USER_NAME'] = $arUserData['FIRST_NAME'] . ' ' . $arUserData['LAST_NAME'];
            $arEventData['C_FIELDS']['EMAIL'] = $arUserData['EMAIL'];
        }

        // ������ ������
        $arSalon = \CDMSSalon::GetList(
            array(),
            array('ID' => $this->arFormData['saloon_id']),
            array('ID', 'NAME', 'PROPERTY_EMAIL')
        )->Fetch();

        $arEventData['C_FIELDS']['EMAIL_TO'] = $arSalon['PROPERTY_EMAIL_VALUE'];
        $arEventData['C_FIELDS']['SALOON_ID'] = $arSalon['ID'];
        $arEventData['C_FIELDS']['SALOON_NAME'] = $arSalon['NAME'];

        // ������ �� �����
        $arEventData['C_FIELDS']['PHONE'] = $this->arFormData['phone'];
        $arEventData['C_FIELDS']['SUBJECT'] = $this->getSubgectValue($this->arFormData['subject']);

        Event::send($arEventData);
    }

    /**
     * ���������� ������ ������������ ���� �� �����������
     * @return array|mixed|null
     */
    protected function getUserData()
    {
        $result = array();
        global $USER;

        if (!empty($USER->GetID())) {
            $nUserId = $USER->GetID();
            $result = \CDFAUsersEntityFactory::getUsers()->getCached(
                array(),
                array('USER_ID' => $nUserId),
                array('FIRST_NAME', 'LAST_NAME', 'EMAIL')
            );
        }
        return $result;
    }

    /**
     * ���������� ��������� �������� �������� "���� �������" �� ���� ��������
     * @param $nPropValId
     * @return bool|mixed
     */
    protected  function getSubgectValue($nPropValId)
    {
        $arProps = $this->getIBlockProps();
        foreach ($arProps as $arProperty) {
            if ($arProperty['CODE'] == 'SUBJECT') {
                $arPropValues = $this->getFieldEnumValues($arProperty);
                return $arPropValues[$nPropValId];
            }
        }
        return false;
    }
}