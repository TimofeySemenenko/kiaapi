<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

use Bitrix\Main\Localization\Loc;

?>
<?php if ($_REQUEST['AJAX_' . $arResult['FORM']['ID']] == 'Y'): ?>
    <?php $GLOBALS['APPLICATION']->RestartBuffer() ?>
<? endif; ?>

<? if ($arResult['SUCCESS'] == 'Y'): ?>
    <div class=""> <!-- TODO: ����� ������ ����� ��� ����� ����� -->
        <div class="o-call-success-title">
            <?= Loc::getMessage('SUCCESS_FORM_SEND_TITLE'); ?>
        </div>
        <div class="o-call-success-body">
            <?= Loc::getMessage('SUCCESS_FORM_SEND_DESCRIPTION'); ?>
        </div>
    </div>
    <? $component->showDealerSuccessFormScripts(); ?>
<? else: ?>
    <? if (!empty($arResult['ERRORS'])): ?>
        <? foreach ($arResult['ERRORS'] as $errorCode => $errorText): ?>
            <p><?= $errorText ?></p><br>
        <? endforeach ?>
    <? endif; ?>

    <div class="o-call" id="wrapper_<?= $arResult['FORM']['ID'] ?>">
        <form class="js-o-call-form"
              method="POST"
              action="<?= $APPLICATION->GetCurDir() ?>"
              name="<?= $arResult['FORM']['NAME'] ?>"
              id="<?= $arResult['FORM']['ID'] ?>">
            <div class="o-call__fields">
                <input type="hidden" name="AJAX_<?= $arResult['FORM']['ID'] ?>" value="Y">
                <?php foreach ($arResult['FIELDS'] as $arField): ?>
                    <? if ($arField['TYPE'] == 'hidden'): ?>
                        <input type="<?= $arField['TYPE'] ?>" name="<?= $arField['HTML_FIELD_NAME'] ?>">
                        <?php continue; ?>
                    <? endif; ?>

                    <div class="o-call__col">
                        <? if ($arField['CODE'] == 'AGREEMENT'): ?>
                            <div class="o-call__checkbox">
                                <span class="o-checkbox__block">
                                <input data-error="true" name="<?= $arField['HTML_FIELD_NAME'] ?>"
                                       class="o-checkbox__el" value="Y" type="checkbox"
                                       id="<?= $arField['HTML_FIELD_ID'] ?>">
                                  <span class="o-checkbox__in">
                                    <svg class="o-checkbox__icon">
                                      <use xlink:href="<?= $templateFolder; ?>/icons/sprite.svg#icon-checkbox"></use>
                                    </svg>
                                  </span>
                                </span>
                                <label for="<?= $arField['HTML_FIELD_ID'] ?>" class="o-checkbox__label">
                                    <?= Loc::getMessage('AGREEMENT_LINK', array('#LINK#' => '#' . $arField['HTML_FIELD_ID'])); ?>
                                </label>
                            </div>
                        <? else: ?>
                            <label for="<?= $arField['HTML_FIELD_ID'] ?>" class="o-call__select-label">
                                <?= Loc::getMessage('ORDER_CALL_FORM_LABEL_' . $arField['CODE']) ?>
                            </label>
                            <div class="o-call__field-container">
                                <div class="o-call__field-side">
                            <span class="o-call__field-icon">
                                <?php switch ($arField['CODE']) {
                                    case 'SUBJECT' :
                                        echo '?';
                                        break;
                                    case 'PHONE' :
                                        echo '+7';
                                        break;
                                    case 'SALOON_ID' :
                                        echo '<svg class="o-call__field-pin"><use xlink:href="' . $templateFolder . '/icons/sprite.svg#icon-pin-thin"></use></svg>';
                                        break;
                                } ?>
                            </span>
                                </div>

                                <?php switch ($arField['CODE']) {
                                    case 'SUBJECT' :
                                        ?>
                                        <div class="select js-select-default">
                                            <select
                                                    data-error="true"
                                                    name="<?= $arField['HTML_FIELD_NAME'] ?>"
                                                    id="<?= $arField['HTML_FIELD_ID'] ?>"
                                                    class="o-call__select-question"
                                                    data-placeholder="<?= Loc::getMessage('ORDER_CALL_FORM_LABEL_' . $arField['CODE']) ?>">
                                                <option></option>
                                                <?
                                                foreach ($arField['VALUES'] as $val => $name):?>
                                                    <option value="<?= $val ?>"<?= ($arField['VALUE'] == $val) ? ' selected' : '' ?>><?= $name ?></option>
                                                <?endforeach ?>
                                            </select>
                                        </div>
                                        <?
                                        break;
                                    case 'PHONE' :
                                        ?>
                                        <input
                                                data-error="true"
                                                id="<?= $arField['HTML_FIELD_ID'] ?>"
                                                class="js-o-call-field js-o-call-field-phone o-call__field"
                                                placeholder="<?= Loc::getMessage('ORDER_CALL_FORM_LABEL_' . $arField['CODE']) ?>"
                                                type="<?= $arField['TYPE'] ?>"
                                                name="<?= $arField['HTML_FIELD_NAME'] ?>"
                                                value="<?= $arField['VALUE'] ?>">
                                        <?
                                        break;
                                    case 'SALOON_ID' :
                                        ?>
                                        <a href="#" class="o-call__dealer js-o-call-dealer popup-link js-popup-link"
                                           data-popup="dealers">
                                    <span class="o-call__field-icon--placeholder">
                                      <svg class="o-call__field-plus">
                                        <use xlink:href="<?= $templateFolder; ?>/icons/sprite.svg#icon-plus"></use>
                                      </svg>
                                    </span>
                                            <?= Loc::getMessage('ORDER_CALL_FORM_LABEL_' . $arField['CODE']) ?>
                                        </a>
                                        <input name="<?= $arField['HTML_FIELD_NAME'] ?>" data-error="true" type="hidden"
                                               id="<?= $arField['HTML_FIELD_ID'] ?>"
                                               class="o-call__field js-test-field">
                                        <?
                                        break;
                                    default :
                                        ?>
                                        <input
                                                data-error="true"
                                                id="<?= $arField['HTML_FIELD_ID'] ?>"
                                                class="js-o-call-field o-call__field"
                                                placeholder="<?= Loc::getMessage('ORDER_CALL_FORM_LABEL_' . $arField['CODE']) ?>"
                                                type="<?= $arField['TYPE'] ?>"
                                                name="<?= $arField['HTML_FIELD_NAME'] ?>">
                                        <?
                                        break;
                                } ?>
                            </div>
                        <? endif; ?>
                    </div>
                <?php endforeach; ?>
            </div>

            <div class="o-call__container-footer">
                <div class="o-call__button">
                    <input class="js-o-button-default-sumbit o-button <?= $component->showGATrackClassByCode('submit_button_click'); ?>"
                           type="submit"
                           value="<?= Loc::getMessage('FORM_SEND_BUTTON') ?>"<?= $component->getGATrackByCode('submit_button_click') ?>>
                </div>
                <div class="o-call__validator js-o-call-validator" style="display: none;">
                    <?= Loc::getMessage(FORM_FIELDS_REQUIRED) ?>:
                </div>
            </div>

            <? if (!empty($arResult['AGREEMENT']['PREVIEW'])): ?>
                <p class="o-call__agreement"><?= $arResult['AGREEMENT']['PREVIEW'] ?></p>
            <? endif; ?>
        </form>
    </div>

    <div style="display: none;">
        <div class="js-o-call-argeement-text" id="<?= $arResult['FIELDS']['AGREEMENT']['HTML_FIELD_ID'] ?>">
            <div class="o-call__agreement">
                <?= $arResult['AGREEMENT']['DETAIL'] ?>
            </div>
        </div>
    </div>

    <? $APPLICATION->IncludeComponent("defa:block", "forms.constructor", array(
        "IBLOCK_TYPE" => "users",
        "IBLOCK_ID" => "152",
        "CACHE_TIME" => "3602",
        "CACHE_TYPE" => "N",
        "SKIP_REDIRECT" => "Y",
        "CLASS_NAME" => $arParams['WRAP_CLASS'],
        "SESSION_ELEMENT_ID_KEY" => "DEALER_POPUP",
        "EVENT_FORM_NAME" => '����� ������',
        "BLOCK_LIST" => "dealer_popup",
        "BLOCK_PARAMS" => array(),
        "BLOCK_dealer_popup_PARAMS" => array(),
    ), $component); ?>
    <script>
        var form_id = '#<?=$arResult['FORM']['ID']?>';

        $(document).ready(function () {
            // ����� ������
            var dealerParams;
            $(document).bind('map-baloon-choose-saloon', function (e, params) {
                dealerParams = params;
                $('.js-popup-link[data-popup="dealers"]').text(params.ar.NAME);
                $('#<?=$arResult['FIELDS']['SALOON_ID']['HTML_FIELD_ID']?>').val(params.id);
                processValidField($('#<?=$arResult['FIELDS']['SALOON_ID']['HTML_FIELD_ID']?>'));
            });

            // ��������� �����, ��������� ��������� � script.js
            var validatorForm;
            var validate_<?=$arResult['FORM']['ID']?> = {
                init: function () {
                    if ($(form_id).length != 0) {
                        validatorForm = $(form_id).validate({
                            submitHandler: function (form) {
                                $.ajax({
                                    type: "POST",
                                    url: '<?=$arResult['FORM']['ACTION']?>',
                                    data: $(form).serialize(),
                                }).done(function (data) {
                                    $('#wrapper_<?=$arResult['FORM']['ID']?>').html(data);
                                });
                            },
                            ignore: [],
                            rules: {
                                <?foreach($arResult['JS_VALIDATE_FORM_DATA']['REQUIRED'] as $id):?>"<?=$id?>": {required: true, <?=(substr($id, -7) == '[phone]') ? 'minlenghtphone: true' : ''?>},
                                <?endforeach?>
                            },
                            messages: {
                                <?foreach($arResult['JS_VALIDATE_FORM_DATA']['MESSAGES'] as $id => $message):?>"<?=$id?>": {required: "<?=$message?>",},
                                <?endforeach?>
                            },
                            success: function (label) {
                                processValidField($('#' + $(label).attr('for')));
                            }
                        });
                    }
                }
            }
            validate_<?=$arResult['FORM']['ID']?>.init();
            $.validator.addMethod("minlenghtphone", function (value, element) {
                if ($(element).hasClass('has-error')) {
                    if ($(form_id).validate().numberOfInvalids() > 0) {
                        return value.replace(/\D+/g, '').length > 9;
                    }
                } else {
                    return true;
                }
            }, "<?=$arResult['FIELDS']['PHONE']['NAME']?>");
        });

        /**
         * �������� ��������� ����
         * @param element
         */
        function processValidField(element) {
            var valid = false;
            if ($(element).val().length > 0) {
                valid = true;
            }
            toggleFieldValid(valid, element)
            checkErrorsBlock();
        }

        /**
         * ����������/�������� ��������� � ������������� ��������� ����
         * @param valid
         * @param element
         */
        function toggleFieldValid(valid, element) {
            if (valid) {
                $('[for=' + $(element).attr('id') + '][generated=true]').hide();
            } else {
                $('[for=' + $(element).attr('id') + '][generated=true]').show();
            }
        }

        /**
         * �������� ���� ����������� � ���������� ����� ���� ������� ���
         */
        function checkErrorsBlock() {
            var errorBlockSelector = '.js-o-call-validator',
                visible = false;

            if ($(errorBlockSelector).is(':visible')) {
                $(errorBlockSelector + ' label.has-error').each(function (i, el) {
                    if ($(el).is(':visible')) {
                        visible = true;
                    }
                });

                if (visible) {
                    $(errorBlockSelector).show();
                } else {
                    $(errorBlockSelector).hide();
                }
            }
        }
    </script>
<? endif; ?>

<?php if ($_REQUEST['AJAX_' . $arResult['FORM']['ID']] == 'Y'): ?>
    <?php die(); ?>
<? endif; ?>


