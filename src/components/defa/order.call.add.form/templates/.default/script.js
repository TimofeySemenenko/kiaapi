'use strict';

var consoleFix = {
  init: function () {
    if (!(window.console && console.log)) {
      (function () {
        var noop = function () {};
        var methods = [
          'assert',
          'clear',
          'count',
          'debug',
          'dir',
          'dirxml',
          'error',
          'exception',
          'group',
          'groupCollapsed',
          'groupEnd',
          'info',
          'log',
          'markTimeline',
          'profile',
          'profileEnd',
          'markTimeline',
          'table',
          'time',
          'timeEnd',
          'timeStamp',
          'trace',
          'warn'];
        var length = methods.length;
        var console = window.console = {};
        while (length--) {
          console[methods[length]] = noop;
        }
      }());
    }
  }
}

var svg = {
  init: function () {
    svg4everybody();
  }
}

var select = {
  init: function ($formElm) {
    //default
    var $parentElement = $formElm.find('.js-select-default');
    var $select = $parentElement.find('select');
    if (($select.length != 0) && (typeof $.fn.select2 == 'function')) {
      $select.each(function () {
        $(this).select2({
          theme: 'default',
          placeholder: $parentElement
        });
      });
      
      $select.on("change", function (e) {
          processValidField($(this));
      });
    }
  }
}

var formInputMaskNoSeven = {
  init: function ($formElm) {
    var $fieldPhone = $formElm.find(".js-o-call-field-phone");
    if ($fieldPhone.length != 0) {

      if ($(".tablet-flag").is(":visible") == false &&
        $(".mobile-flag").is(":visible") == false) {
        $fieldPhone.mask("(999) 999-99-99");
      } else {
        $fieldPhone.mask('9999999999');
      }
    }
  }
}

var fancyBoxAgreement = {
  init: function ($formElm) {
    if ($(".js-o-call-argeement-text").length != 0) {

      $(document).on("click", form_id + " .js-o-call-agreement", function (e) {
              e.preventDefault();
              $.fancybox($.extend(
                  {},
                  fancyDefaultPhotoParams,
                  {
                      content: $('.js-o-call-argeement-text').html(),
                      overlayColor: "#000",
                      overlayOpacity: 0.85,
                      onStart: function() {
                          $("#fancybox-wrap").addClass("popup-area popup-greenment-inline-block");
                          $('body').addClass('popuped');
                      },
                      onComplete: function() {
                          $("#fancybox-wrap.popup-area #fancybox-content *").filter("[id][id!=''], [for]").each(function() {
                              if (typeof(this.id) == "string" && this.id.length > 0)
                                  this.id += "_popup";
                              if (typeof(this.htmlFor) == "string")
                                  this.htmlFor += "_popup";
                          });
                      },
                      onClosed: function () {
                          $('body').removeClass('popuped');

                      },
                      transitionIn: "none",
                      transitionOut: "none",
                      padding: 0,
                      margin: 0,
                      hideOnOverlayClick: true
                  }
              ));
          });
    }
  }
}

var oCallpopupOpen = {
  init: function ($formElm) {
    if ($formElm.closest('#js-o-call-popupform').length != 0) {
      
      $.validator.setDefaults({
        errorClass: 'has-error',
        highlight: function (element) {
          if ($(element).parent().hasClass('js-select-default')) {
            $(element).parent().find('.selection').addClass('has-error');
          } else if ($(element).hasClass('o-checkbox__el')) {
            $(element).
              parent('.o-checkbox__block').
              find('.o-checkbox__in').
              addClass('has-error');
          } else {
            $(element).addClass('has-error');
          }
        },
        unhighlight: function (element) {
          if ($(element).parent().hasClass('js-select-default')) {
            $(element).parent().find('.selection').removeClass('has-error');
          } else {
            $(element).removeClass('has-error');
          }
        },
        errorPlacement: function (error, element) {
        
        }
        
      });
      
      $(".js-o-call-button").fancybox({
            fitToView : false,
            autoSize  : false,
            closeClick  : false,
            openEffect  : 'none',
            closeEffect : 'none',
            padding: 0,
            margin: 0,
            type : 'inline',
            showCloseButton: false,
            onStart: function () {
                $("#fancybox-wrap").removeClass("popup-area popup-greenment-inline-block");
                $('body').addClass('popuped');
                $('#fancybox-close').hide();
            },
            onComplete: function () {
                $('#fancybox-close').hide();
            },
            onClosed: function () {
                $('body').removeClass('popuped');
            }
        });
        $('body').on('click', '.b-popup__fog', function (e) {
            $.fancybox.close();
        });
      
      $('.js-o-call-popup-form').validate({
        submitHandler: function (form) {
          $.fancybox.open({
            src: '#js-o-call-success',
            type: 'inline',
          });
          form.submit();
        },
        ignore: [],
        rules: {
          tel2: {
            required: true,
          },
          question2: {
            required: true,
          },
          checkbox2: {
            required: true,
          },
          dealer2: {
            required: true,
          }
        },
        
      });
    }
  }
}

var fieldSideToggle = {
  init: function ($formElm) {
    $formElm.find($(".o-call__field-side")).hide();
    $formElm.find(".js-o-call-field").focus(function () {
      $(this).parent().find('.o-call__field-side').show();
    });
    $formElm.find(".js-select-default").change(function () {
      $(this).parent().find(".o-call__field-side").show();
      $(this).parent().find('.selection').removeClass('has-error');
    });
  }
}

var fieldSidePopupToggle = {
  init: function ($formElm) {
    $(".o-call__field-popup-side").hide();
    $(".o-call__popup-field").focus(function () {
      $(this).parent().find('.o-call__field-side').show();
    });
    $(".js-select-default").change(function () {
      $(this).parent().find(".o-call__field-side").show();
      $(this).parent().find('.selection').removeClass('has-error');
    });
  }
}

var validatorHide = {
  init: function () {
    $formElm.find('.o-call__validator').hide();
  }
}

//document ready

$(function () {
  $formElm = $(form_id);

  consoleFix.init();
  svg.init();
  select.init($formElm);
  formInputMaskNoSeven.init($formElm);
  
  fieldSideToggle.init($formElm);
  validatorHide.init($formElm);
  // fancyBoxDealers.init();
  fancyBoxAgreement.init($formElm);
  oCallpopupOpen.init($formElm);
  fieldSidePopupToggle.init($formElm);
  
  $.validator.setDefaults({
    errorClass: 'has-error',
    highlight: function (element) {
      if ($(element).parent().hasClass('js-select-default')) {
        $(element).parent().find('.selection').addClass('has-error');
      } else if ($(element).hasClass('o-checkbox__el')) {
        $(element).
          parent('.o-checkbox__block').
          find('.o-checkbox__in').
          addClass('has-error');
      } else {
        $(element).addClass('has-error');
      }
    },
    unhighlight: function (element) {
      if ($(element).hasClass('js-o-call-field-phone') && $(element).val().replace(/\D+/g, '').length != 10){
        return false;
      }
      if ($(element).parent().hasClass('js-select-default')) {
        $(element).parent().find('.selection').removeClass('has-error');
      }  else {
        $(element).removeClass('has-error');
      }
      if(this.numberOfInvalids() == 0){
        $('.js-o-call-validator').hide();
      }
      $('.js-o-call-validator label[for=' + $(element).attr('id') + ']').remove();
    },
    errorPlacement: function (error, element) {
      if (element.attr("data-error") == "true") {
        /** workaround
         * �� ���������� ������� ��� �������� ���������
         * � label ������������� ����������� � �������� for ���������� name, � �� id
         * ������� ���� ������� �� ������ - ���� ��� �� name
         */
        if ($(error).attr('for') !== $(element).attr('id')) {
          $(error).attr('for', $(element).attr('id'));
        }
        error.appendTo('.js-o-call-validator');
        $('.js-o-call-validator').show();
      }
    }
  });
});
