<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}
use Bitrix\Main\Page\Asset;

DSiteTemplate::SetAdditionalCSS($templateFolder . '/styles/style.less');

Asset::getInstance()->addJs('/bitrix/js/jquery/prop_phone/jquery.maskedinput-1.3.min.js');
Asset::getInstance()->addJs('/bitrix/templates/.default/js/svg4everybody.min.js');
Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/main/script/plugins/select2.full.min.js');
Asset::getInstance()->addJs('/bitrix/templates/.default/js/jquery.validate.js');
Asset::getInstance()->addJs('/bitrix/templates/.default/js/fancybox2/jquery.fancybox.pack.js');