<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}
use Bitrix\Main\Localization\Loc;
?>
<div class="lego__wrapper lego__order-cell-wrap">
    <asside class="lego__order-cell">
        <h1 class="lego__order-cell-title"><?=(!empty($arParams['TEXT_FORM'])) ?$arParams['TEXT_FORM'] :'�������� ������'?></h1>
        <form action="" class="lego__order-cell-body lego__flex-col">
            <div class="lego__order-cell-input-wrap lego__flex-col">
                <input type="text" class="lego__order-cell-input" name="name" placeholder="���" required>
                <span class="lego__order-cell-hint"></span>
                <span class="lego__order-cell-err">������� ���� ���</span>
            </div>
            <div class="lego__order-cell-input-wrap lego__flex-col">
                <input type="tet" class="lego__order-cell-input lego__order-cell-input--tel" name="phone"
                       placeholder="�����"  required>
                <span class="lego__order-cell-hint">��� ����� ��������</span>
                <span class="lego__order-cell-err">������� ��� ����� ��������</span>
            </div>
            <a class="lego__order-cell-agreement"><span class="underline">�������� ��
                            ���������&nbsp;����� ������������
                            �����x</span></a>
            <button type="submit" class="lego__order-cell-send"><?=(!empty($arParams['BUTTON_FORM'])) ? $arParams['BUTTON_FORM'] :'���������'?></button>
        </form>
    </asside>
</div>

<script type="text/javascript">
  $('.lego__order-cell-input--tel').unmask().mask("+7 (999) 999-99-99", {});
</script>