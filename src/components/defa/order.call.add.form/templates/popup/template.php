<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

use Bitrix\Main\Localization\Loc;

?>

<? if ($component->bShowButton()): ?>
    <a class="o-call-button-fixed js-o-call-button" href="#js-o-call-popupform" style="display: none">
        <svg class="o-call-button__icon">
            <use xlink:href="<?= $templateFolder; ?>/icons/sprite.svg#icon-tel"></use>
        </svg>
        <span>
                <?= Loc::getMessage('BUTTON_CALL_TEXT'); ?>
            </span>
    </a>
<? endif; ?>

<?php if ($_REQUEST['AJAX_' . $arResult['FORM']['ID']] == 'Y'): ?>
    <?php $GLOBALS['APPLICATION']->RestartBuffer() ?>
<? endif; ?>

<? if ($arResult['SUCCESS'] == 'Y'): ?>
    <div class=""> <!-- TODO: ����� ������ ����� ��� ����� ����� -->
        <div class="o-call-success-title">
            <div class="o-call-success-icon">
                <svg class="icon">
                    <use xlink:href="<?= $templateFolder; ?>/icons/sprite.svg#icon-checkbox"></use>
                </svg>
            </div>
            <?= Loc::getMessage('SUCCESS_FORM_SEND_TITLE'); ?>
        </div>
        <div class="o-call-success-body">
            <?= Loc::getMessage('SUCCESS_FORM_SEND_DESCRIPTION'); ?>
        </div>
    </div>
    <? $component->showDealerSuccessFormScripts(); ?>
<? else: ?>
    <? if (!empty($arResult['ERRORS'])): ?>
        <? foreach ($arResult['ERRORS'] as $errorCode => $errorText): ?>
            <p><?= $errorText ?></p><br>
        <? endforeach ?>
    <? endif; ?>

    <div id="js-o-call-popupform" class="o-call__popup">
        <div>
            <div class="o-call__popup-title"><?= Loc::getMessage('FORM_CALL_TITLE'); ?></div>
            <div id="wrapper_<?= $arResult['FORM']['ID'] ?>">
                <form class="o-call-popupform js-o-call-popup-form"
                      method="POST"
                      action="<?= $APPLICATION->GetCurDir() ?>"
                      name="<?= $arResult['FORM']['NAME'] ?>"
                      id="<?= $arResult['FORM']['ID'] ?>">

                    <div class="o-call__popup-fields">
                        <input type="hidden" name="AJAX_<?= $arResult['FORM']['ID'] ?>" value="Y">
                        <?php foreach ($arResult['FIELDS'] as $arField): ?>
                            <? if ($arField['TYPE'] == 'hidden'): ?>
                                <input type="<?= $arField['TYPE'] ?>" name="<?= $arField['HTML_FIELD_NAME'] ?>">
                                <?php continue; ?>
                            <? endif; ?>

                            <? if ($arField['CODE'] == 'AGREEMENT'): ?>
                                <div class="o-call__checkbox o-call__popup-checkbox">
                                    <span class="o-checkbox__block">
                                        <input name="<?= $arField['HTML_FIELD_NAME'] ?>" class="o-checkbox__el"
                                               value="Y"
                                               type="checkbox" id="<?= $arField['HTML_FIELD_ID'] ?>">
                                        <span class="o-checkbox__in">
                                            <svg class="o-checkbox__icon">
                                                <use xlink:href="<?= $templateFolder; ?>/icons/sprite.svg#icon-checkbox"></use>
                                            </svg>
                                        </span>
                                    </span>
                                    <label for="<?= $arField['HTML_FIELD_ID'] ?>" class="o-checkbox__label">
                                        <?= Loc::getMessage('AGREEMENT_LINK', array('#LINK#' => '#' . $arField['HTML_FIELD_ID'], '#CLASS#' => 'o-checkbox__red js-o-call-agreement')); ?>
                                    </label>
                                </div>
                            <? else: ?>
                                <div class="o-call__field-container o-call__popup-field-container">
                                    <div class="o-call__field-side">
                                        <span class="o-call__field-icon">
                                         <?php switch ($arField['CODE']) {
                                             case 'SUBJECT' :
                                                 echo '?';
                                                 break;
                                             case 'PHONE' :
                                                 echo '+7';
                                                 break;
                                             case 'SALOON_ID' :
                                                 echo '<svg class="o-call__field-pin"><use xlink:href="' . $templateFolder . '/icons/sprite.svg#icon-pin-thin"></use></svg>';
                                                 break;
                                         } ?>
                                    </span>
                                    </div>
                                    <?php switch ($arField['CODE']) {
                                        case 'SUBJECT' :
                                            ?>
                                            <div class="select js-select-default">
                                                <select
                                                        data-error="true"
                                                        name="<?= $arField['HTML_FIELD_NAME'] ?>"
                                                        id="<?= $arField['HTML_FIELD_ID'] ?>"
                                                        class="o-call__select-question"
                                                        data-placeholder="<?= Loc::getMessage('ORDER_CALL_FORM_LABEL_' . $arField['CODE']) ?>">
                                                    <option></option>
                                                    <?
                                                    foreach ($arField['VALUES'] as $val => $name):?>
                                                        <option value="<?= $val ?>"<?= ($arField['VALUE'] == $val) ? ' selected' : '' ?>><?= $name ?></option>
                                                    <? endforeach ?>
                                                </select>
                                            </div>
                                            <?
                                            break;
                                        case 'PHONE' :
                                            ?>
                                            <input
                                                    data-error="true"
                                                    id="<?= $arField['HTML_FIELD_ID'] ?>"
                                                    class="js-o-call-field o-call__field js-o-call-field-phone"
                                                    placeholder="<?= Loc::getMessage('ORDER_CALL_FORM_LABEL_' . $arField['CODE']) ?>"
                                                    type="<?= $arField['TYPE'] ?>"
                                                    name="<?= $arField['HTML_FIELD_NAME'] ?>"
                                                    value="<?= $arField['VALUE'] ?>">
                                            <?
                                            break;
                                        case 'SALOON_ID' :
                                            ?>
                                            <input name="<?= $arField['HTML_FIELD_NAME'] ?>" data-error="true"
                                                   type="hidden" id="<?= $arField['HTML_FIELD_ID'] ?>"
                                                   class="o-call__field">
                                            <a href="#" class="o-call__dealer js-popup-o-call-dealer"
                                               data-popup="dealers">
                                                <?= Loc::getMessage('ORDER_CALL_FORM_LABEL_' . $arField['CODE']) ?>
                                            </a>

                                            <?
                                            break;
                                        default :
                                            ?>
                                            <input
                                                    data-error="true"
                                                    id="<?= $arField['HTML_FIELD_ID'] ?>"
                                                    class="js-o-call-field o-call__field"
                                                    placeholder="<?= Loc::getMessage('ORDER_CALL_FORM_LABEL_' . $arField['CODE']) ?>"
                                                    type="<?= $arField['TYPE'] ?>"
                                                    name="<?= $arField['HTML_FIELD_NAME'] ?>">
                                            <?
                                            break;
                                    } ?>
                                </div>
                            <? endif; ?>
                        <?php endforeach; ?>

                        <div class="o-call__popup-button-container">
                            <div class="o-call__button o-call__popup-button">
                                <input class="o-button gatrack"
                                       type="submit"
                                       value="<?= Loc::getMessage('FORM_SEND_BUTTON') ?>">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div style="display: none;">
        <div class="js-o-call-argeement-text" id="<?= $arResult['FIELDS']['AGREEMENT']['HTML_FIELD_ID'] ?>">
            <div class="o-call__agreement">
                <?= $arResult['AGREEMENT']['DETAIL'] ?>
            </div>
        </div>
    </div>
    <?
    if ($arParams['INIT_DEALERS_MAP']) {
        $APPLICATION->IncludeComponent("defa:block", "forms.constructor", array(
            "IBLOCK_TYPE" => "users",
            "IBLOCK_ID" => "152",
            "CACHE_TIME" => "3602",
            "CACHE_TYPE" => "N",
            "SKIP_REDIRECT" => "Y",
            "CLASS_NAME" => $arParams['WRAP_CLASS'],
            "SESSION_ELEMENT_ID_KEY" => "DEALER_POPUP",
            "EVENT_FORM_NAME" => '����� ������',
            "BLOCK_LIST" => "dealer_popup",
            "BLOCK_PARAMS" => array(),
            "BLOCK_dealer_popup_PARAMS" => $arParams['DEALER_MAP_PARAMS'],
        ), $component);
    }

    ?>
    <script>
        var form_id = '#<?=$arResult['FORM']['ID']?>';

        var dataCategoryLabel = '����� - ����� ������';
        var dataOptLabel_formOpen = '����� ����� ������ ������';
        var dataOptLabel_formSend = '�������� ������ ������';
        var dataOptLabel_formSuccess = '�������� �������� ������ ������';
        var dataOptLabel_dealerSelected = '������� ������';
        var $callBtn;

        function setButtonAttrs($element, label) {
            $element.attr('data-category', dataCategoryLabel);
            $element.attr('data-action', $callBtn.attr('data-action'));
            $element.attr('data-opt_label', label);
        }

        orderCallScript(form_id);

        $(document).ready(function () {
            var validate_<?=$arResult['FORM']['ID']?> = {
                init: function () {
                    if ($(form_id).length != 0) {
                        $(form_id).validate({
                            submitHandler: function (form) {
                                $.ajax({
                                    type: "POST",
                                    url: '<?=$arResult['FORM']['ACTION']?>',
                                    data: $(form).serialize(),
                                }).done(function (data) {
                                    $('#wrapper_<?=$arResult['FORM']['ID']?>').html(data);
                                    ODFAGATracker.trackEvent(dataCategoryLabel, $callBtn.attr('data-action'), dataOptLabel_formSuccess);
                                });
                            },
                            ignore: [],
                            rules: {
                                <?foreach($arResult['JS_VALIDATE_FORM_DATA']['REQUIRED'] as $id):?>"<?=$id?>": {required: true},
                                <?endforeach?>
                            },
                            messages: {
                                <?foreach($arResult['JS_VALIDATE_FORM_DATA']['MESSAGES'] as $id => $message):?>"<?=$id?>": {required: "<?=$message?>",},
                                <?endforeach?>
                            }
                        });
                    }
                }
            }
            validate_<?=$arResult['FORM']['ID']?>.init();

            $(document).off('click', '.js-popup-o-call-dealer').on('click', '.js-popup-o-call-dealer', function(e) {
                e.preventDefault();
                $.fancyboxNew.open('#popup-box-dealers', {
                    afterLoad: function() {
                    },
                    afterClose: function() {
                        setTimeout(function(){
                            $.fancyboxNew.open('#js-o-call-popupform')
                        }, 500)
                    }
                });
            });

            var dealerParams;
            $(document).bind('map-baloon-choose-saloon', function (e, params) {
                dealerParams = params;
                // ������� ����� ������ �� ��������� ����� �� ��������� ��������� ������ � ������ ������ ������
                if ($('.js-popup-link[data-popup="dealers"]').length > 1) {
                    setTimeout(function () {
                        $(form_id + ' .js-popup-link[data-popup="dealers"]').text(params.ar.NAME);
                        ODFAGATracker.trackEvent(dataCategoryLabel, $callBtn.attr('data-action'), dataOptLabel_dealerSelected);
                    }, 1000);
                } else {
                    $(form_id + ' .js-popup-link[data-popup="dealers"]').text(params.ar.NAME);
                }
                $('#<?=$arResult['FIELDS']['SALOON_ID']['HTML_FIELD_ID']?>').val(params.id);
                $('#<?=$arResult['FIELDS']['SALOON_ID']['HTML_FIELD_ID']?>').removeClass('has-error');
            });
        });
    </script>
<? endif; ?>

<?php if ($_REQUEST['AJAX_' . $arResult['FORM']['ID']] == 'Y'): ?>
    <?php die(); ?>
<? endif; ?>


