'use strict';

var consoleFix = {
  init: function () {
    if (!(window.console && console.log)) {
      (function () {
        var noop = function () {};
        var methods = [
          'assert',
          'clear',
          'count',
          'debug',
          'dir',
          'dirxml',
          'error',
          'exception',
          'group',
          'groupCollapsed',
          'groupEnd',
          'info',
          'log',
          'markTimeline',
          'profile',
          'profileEnd',
          'markTimeline',
          'table',
          'time',
          'timeEnd',
          'timeStamp',
          'trace',
          'warn'];
        var length = methods.length;
        var console = window.console = {};
        while (length--) {
          console[methods[length]] = noop;
        }
      }());
    }
  }
}

var svg = {
  init: function () {
    svg4everybody();
  }
}

var select = {
  init: function ($formElm) {
    //default
    var $parentElement = $formElm.find('.js-select-default');
    var $select = $parentElement.find('select');
    if (($select.length != 0) && (typeof $.fn.select2 == 'function')) {
      $select.each(function () {
        $(this).select2({
          theme: 'default',
          placeholder: $(this).attr('data-placeholder'),
            dropdownParent: $parentElement
        });
      });
    }
  }
}

var formInputMaskNoSeven = {
  init: function ($formElm) {
    var $field = $formElm.find('.js-o-call-field');
    if ($field.length != 0) {
        $field.unmask();
      if ($(".tablet-flag").is(":visible") == false &&
        $(".mobile-flag").is(":visible") == false) {
        $field.mask("(999) 999-99-99");
      } else {
        $field.mask('9999999999');
      }
    }
  }
}

var fancyBoxAgreement = {
  init: function ($formElm) {
    if ($(".js-o-call-argeement-text").length != 0) {

      $(document).on("click", form_id + " .js-o-call-agreement", function (e) {
              e.preventDefault();
              $.fancybox($.extend(
                  {},
                  fancyDefaultPhotoParams,
                  {
                      content: $('.js-o-call-argeement-text').html(),
                      overlayColor: "#000",
                      overlayOpacity: 0.85,
                      onStart: function() {
                          $("#fancybox-wrap").addClass("popup-area popup-greenment-inline-block");
                      },
                      onComplete: function() {
                          $("#fancybox-wrap.popup-area #fancybox-content *").filter("[id][id!=''], [for]").each(function() {
                              if (typeof(this.id) == "string" && this.id.length > 0)
                                  this.id += "_popup";
                              if (typeof(this.htmlFor) == "string")
                                  this.htmlFor += "_popup";
                          });
                      },
                      transitionIn: "none",
                      transitionOut: "none",
                      padding: 0,
                      margin: 0,
                      hideOnOverlayClick: true
                  }
              ));
          });
    }
  }
}

var oCallpopupFancyParams = {
    openMethod : 'dropIn',
    openSpeed : 500,
    openTop : 500,
    closeMethod : 'dropOut',
    closeSpeed : 500,
    padding: 0,
    openOpacity  : true,
    closeOpacity  : true,
    closeClick : false,
    afterClose: function () {
        $('body').removeClass('popuped');
    },
    beforeLoad: function () {
        var $formElm = $(form_id);
        $("#fancybox-wrap").removeClass("popup-area popup-greenment-inline-block");
        $('body').addClass('popuped');
        $('#fancybox-close').hide();
        select.init($formElm);
        formInputMaskNoSeven.init($formElm);
    },
};


var oCallpopupOpen = {
  init: function ($formElm) {

    if ($formElm.closest('#js-o-call-popupform').length != 0) {
      $(".js-o-call-button").each(function () {
        $(this).attr('href', '#js-o-call-popupform');
      })
    } 

      $('.js-o-call-button').fancyboxNew(
          $.extend(
              oCallpopupFancyParams,
              {
                  onStart: function () {
                      $("#fancybox-wrap").removeClass("popup-area popup-greenment-inline-block");
                      $('body').addClass('popuped');
                      $('#fancybox-close').hide();                        
                  },
                  onComplete: function () {
                      $('#fancybox-close').hide();
                  },
                  onClosed: function () {
                      $('body').removeClass('popuped');
                  },
                  afterShow: function() {
                    $callBtn = this.element;
                    setButtonAttrs($formElm.find('.o-button'), dataOptLabel_formSend);
                  }
              }
          )
      );

      $('body').on('click', '.b-popup__fog', function (e) {
          // $.fancybox.close();
          $.fancyboxNew.close();
      });

        $('.fancybox-new-close').on('click', function (e) {
            $('.fancybox-new-overlay').attr('style', 'display: none');
        })
      
  }
}



var fieldSideToggle = {
  init: function ($formElm) {
    $formElm.find($(".o-call__field-side")).hide();
    $formElm.find(".js-o-call-field").focus(function () {
      $(this).parent().find('.o-call__field-side').show();
    });
    $formElm.find(".js-select-default").change(function () {
      $(this).parent().find(".o-call__field-side").show();
      $(this).parent().find('.selection').removeClass('has-error');
    });
  }
}

var fieldSidePopupToggle = {
  init: function ($formElm) {
    $(".o-call__field-popup-side").hide();
    $(".o-call__popup-field").focus(function () {
      $(this).parent().find('.o-call__field-side').show();
    });
    $(".js-select-default").change(function () {
      $(this).parent().find(".o-call__field-side").show();
      $(this).parent().find('.selection').removeClass('has-error');
    });
  }
}

var validatorHide = {
  init: function ($formElm) {
    $formElm.find('.o-call__validator').hide();
  }
}

//document ready

function orderCallScript(form_id) {
  $(function() {
    var $formElm = $(form_id);

    consoleFix.init();
    svg.init();
    select.init($formElm);
    formInputMaskNoSeven.init($formElm);
    
    fieldSideToggle.init($formElm);
    validatorHide.init($formElm);
    // fancyBoxDealers.init();
    fancyBoxAgreement.init($formElm);
    oCallpopupOpen.init($formElm);
    fieldSidePopupToggle.init($formElm);
    
    $.validator.setDefaults({
      errorClass: 'has-error',
      highlight: function (element) {
        if ($(element).parent().hasClass('js-select-default')) {
          $(element).parent().find('.selection').addClass('has-error');
        } else if ($(element).hasClass('o-checkbox__el')) {
          $(element).
            parent('.o-checkbox__block').
            find('.o-checkbox__in').
            addClass('has-error');
        } else {
          $(element).addClass('has-error');
        }
      },
      unhighlight: function (element) {
        if ($(element).hasClass('js-o-call-field-phone') && $(element).val().replace(/\D+/g, '').length != 10){
          return false;
        }
        if ($(element).parent().hasClass('js-select-default')) {
          $(element).parent().find('.selection').removeClass('has-error');
        } else {
          $(element).removeClass('has-error');
        }
      },
      errorPlacement: function (error, element) {
        if (element.attr("data-error") == "true") {
          error.appendTo('.js-o-call-validator');
          $('.js-o-call-validator').show();
        }
      }
    });
  })
}
